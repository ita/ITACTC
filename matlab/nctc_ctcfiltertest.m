%% Folder with WAV export data
export_folder = '../build/NCTC_CTCFilterTest/debug_x64';


%% Load
hrtf_ls1_cpp = ita_read( fullfile( export_folder, 'HRTF_1.wav' ) );
hrtf_ls1_cpp.signalType = 'energy';
hrtf_ls1_cpp.comment = 'HRTF LS1 CPP Impl';
hrtf_ls1_cpp.channelNames = { 'LS1 Left', 'LS1 Right' };
hrtf_ls2_cpp = ita_read( fullfile( export_folder, 'HRTF_2.wav' ) );
hrtf_ls2_cpp.signalType = 'energy';
hrtf_ls2_cpp.comment = 'HRTF LS CPP Impl';
hrtf_ls2_cpp.channelNames = { 'LS1 Left', 'LS1 Right' };


ctcfilter_ls1_cpp = ita_read( fullfile( export_folder, 'CTCFilter_ls1.wav' ) );
ctcfilter_ls1_cpp.signalType = 'energy';
ctcfilter_ls1_cpp.comment = 'LS1 CPP Impl';
ctcfilter_ls1_cpp.channelNames = { 'LS1 Left', 'LS1 Right' };
ctcfilter_ls2_cpp = ita_read( fullfile( export_folder, 'CTCFilter_ls2.wav' ) );
ctcfilter_ls2_cpp.signalType = 'energy';
ctcfilter_ls2_cpp.comment = 'LS2 CPP Impl';
ctcfilter_ls2_cpp.channelNames = { 'LS2 Left', 'LS2 Right' };


%% Calculate with MATLAB
[ ctcfilter_matlab_raw, Horig ] = ita_CTC_filter( hrtf_ls1_cpp, hrtf_ls2_cpp );

hrtf_ls1_matlab = ita_merge( Horig(1,1), Horig(2,1) );
hrtf_ls1_matlab.signalType = 'energy';
hrtf_ls1_matlab.comment = 'HRTF LS1 MATLAB Impl';
hrtf_ls1_matlab.channelNames = { 'LS1 Left', 'LS1 Right' };
hrtf_ls2_matlab = ita_merge( Horig(1,2), Horig(2,2) );
hrtf_ls2_matlab.signalType = 'energy';
hrtf_ls2_matlab.comment = 'HRTF LS2 MATLAB Impl';
hrtf_ls2_matlab.channelNames = { 'LS1 Left', 'LS1 Right' };

ctcfilter_ls1_matlab = ita_merge( ctcfilter_matlab_raw(1,1), ctcfilter_matlab_raw(2,1) );
ctcfilter_ls1_matlab.signalType = 'energy';
ctcfilter_ls1_matlab.comment = 'LS1 MATLAB Impl';
ctcfilter_ls1_matlab.channelNames = { 'LS1 Left', 'LS1 Right' };
ctcfilter_ls2_matlab = ita_merge( ctcfilter_matlab_raw(1,2), ctcfilter_matlab_raw(2,2) );
ctcfilter_ls2_matlab.signalType = 'energy';
ctcfilter_ls2_matlab.comment = 'LS2 MATLAB Impl';
ctcfilter_ls2_matlab.channelNames = { 'LS2 Left', 'LS2 Right' };


%% Plot
hrtf_cpp = ita_merge( hrtf_ls1_cpp, hrtf_ls2_cpp );
hrtf_cpp.comment = 'HRTF CPP Impl';
hrtf_cpp.pf

hrtf_matlab = ita_merge( hrtf_ls1_matlab, hrtf_ls2_matlab );
hrtf_matlab.comment = 'HRTF MATLAB Impl';
hrtf_matlab.pf

ctcfilter_cpp = ita_merge( ctcfilter_ls1_cpp, ctcfilter_ls2_cpp );
ctcfilter_cpp.comment = 'CTC Filter CPP Impl';
ctcfilter_cpp.pf

ctcfilter_matlab = ita_merge( ctcfilter_ls1_matlab, ctcfilter_ls2_matlab );
ctcfilter_matlab.comment = 'CTC Filter MATLAB Impl';
ctcfilter_matlab.pf
