#include <DAFF.h>
#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAFFTUtils.h>
#include <ITAHDFTSpectrum.h>
#include <ITANCTC.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <assert.h>
#include <cmath>
#include <complex>
#include <iostream>


ITANCTC::ITANCTC( const Config& oNCTCConfig ) : m_oConfig( oNCTCConfig ), m_pHRIR( NULL )
{
	if( oNCTCConfig.N <= 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Wrong configuration, N-CTC requires a valid number of loudspeakers" );

	m_iOptimization                         = m_oConfig.iOptimization;
	m_fCrossTalkCancellationFactor          = m_oConfig.fCrossTalkCancellationFactor;
	m_fWaveIncidenceAngleCompensationFactor = m_oConfig.fWaveIncidenceAngleCompensationFactor;
	m_fRegularizationFactor                 = oNCTCConfig.fRegularizationFactor;

	m_oHeadPose.vPos.SetToZeroVector( );
	m_oHeadPose.vView.SetValues( 0, 0, -1.0f );
	m_oHeadPose.vUp.SetValues( 0, 1.0f, 0 );
	// m_oHeadPose.qOrient.SetToNeutralQuaternion();

	m_sfCTC_temp.Init( 2, m_oConfig.iCTCFilterLength, true );

	int iDFTSize = m_oConfig.iCTCFilterLength + 1;
	for( int n = 0; n < GetNumChannels( ); n++ )
	{
		m_vfWeights.push_back( 1.0f );
		m_vpHRTFs.push_back( new ITABase::CHDFTSpectra( m_oConfig.fSampleRate, 2, iDFTSize, true ) );
		m_vfDelayTime.push_back( float( m_oConfig.iCTCFilterLength ) / m_oConfig.fSampleRate / 2.0f );
	}

	for( int i = 0; i < 2; i++ )
		m_vpHelper2x2.push_back( new ITABase::CHDFTSpectra( m_oConfig.fSampleRate, 2, iDFTSize, true ) );

	t   = new ITABase::CHDFTSpectrum( m_oConfig.fSampleRate, iDFTSize, true );
	det = new ITABase::CHDFTSpectrum( m_oConfig.fSampleRate, iDFTSize, true );

	m_fft.plan( ITAFFT::FFT_R2C, m_oConfig.iCTCFilterLength, m_sfCTC_temp[0].GetData( ), ( *m_vpHRTFs[0] )[0]->GetData( ) );
	m_ifft.plan( ITAFFT::IFFT_C2R, m_oConfig.iCTCFilterLength, ( *m_vpHRTFs[0] )[0]->GetData( ), m_sfCTC_temp[0].GetData( ) );
}

ITANCTC::~ITANCTC( )
{
	for( int n = 0; n < GetNumChannels( ); n++ )
		delete m_vpHRTFs[n];

	for( int i = 0; i < 2; i++ )
		delete m_vpHelper2x2[i];

	delete t, det;

	return;
}

const ITANCTC::Config& ITANCTC::GetConfig( ) const
{
	return m_oConfig;
}

int ITANCTC::GetNumChannels( ) const
{
	return m_oConfig.N;
}

void ITANCTC::UpdateHeadPose( const Pose& oHead )
{
	m_oHeadPose = oHead;

	return;
}

int ITANCTC::GetLoudspeakerSide( const int iNumLoudspeaker ) const
{
	Pose oDest = GetLoudspeakerPose( iNumLoudspeaker );

	double dPhiDeg, dThetaDeg;

	VistaVector3D vDir = oDest.vPos - m_oHeadPose.vPos;
	vDir.Normalize( );
	VistaVector3D vViewMinusZ     = m_oHeadPose.vView * ( -1.0f );        // local z axis
	const VistaVector3D vRight    = vViewMinusZ.Cross( m_oHeadPose.vUp ); // local x axis
	const double dAzimuthAngleDeg = atan2( vDir.Dot( vRight ), vDir.Dot( m_oHeadPose.vView ) ) * 180.0f / ITAConstants::PI_D;
	dPhiDeg                       = ( ( dAzimuthAngleDeg < 0.0f ) ? ( dAzimuthAngleDeg + 360.0f ) : dAzimuthAngleDeg );
	dThetaDeg                     = asin( vDir.Dot( m_oHeadPose.vUp ) ) * 180.0f / ITAConstants::PI_D;
	if( dPhiDeg < 180 )
	{
		return Config::Loudspeaker::LEFT_SIDE;
	}
	else
	{
		return Config::Loudspeaker::RIGHT_SIDE;
	}
}

void ITANCTC::AddHRIR( const Pose& oDest, ITASampleFrame& sfDestHRIR, bool& bOutOfRange, const double dReflectionFactor /* = 1.0f */,
                       const int iDistanceCompensationSamples /* = 0 */ ) const
{
	if( sfDestHRIR.channels( ) != 2 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Two channel HRIR expected" );
	if( !m_pHRIR )
		ITA_EXCEPT1( MODAL_EXCEPTION, "HRIR missing" );

	// Calculate sample delay of HRIR insertion position
	VistaVector3D v3Conn   = oDest.vPos - m_oHeadPose.vPos;
	double dDistanceMeters = double( v3Conn.GetLength( ) );

	// 1/r attenuation due to distance law
	// Note: Gain of HRIR dataset is normalized to 1 m according to convention
	float fDistanceGain = 1 / (float)dDistanceMeters;

	double dPhiDeg, dThetaDeg;

	VistaVector3D vDir            = v3Conn.GetNormalized( );
	VistaVector3D vViewMinusZ     = m_oHeadPose.vView * ( -1.0f );        // local z axis
	const VistaVector3D vRight    = vViewMinusZ.Cross( m_oHeadPose.vUp ); // local x axis
	const double dAzimuthAngleDeg = atan2( vDir.Dot( vRight ), vDir.Dot( m_oHeadPose.vView ) ) * 180.0f / ITAConstants::PI_D;
	dPhiDeg                       = ( ( dAzimuthAngleDeg < 0.0f ) ? ( dAzimuthAngleDeg + 360.0f ) : dAzimuthAngleDeg );
	dThetaDeg                     = asin( vDir.Dot( m_oHeadPose.vUp ) ) * 180.0f / ITAConstants::PI_D;

	// Determine offset of target IR subtracting also the minimum distance to all LS
	int iHRIRPreOffset       = m_pHRIR->getMinEffectiveFilterOffset( );
	int iHRIRFilerTaps       = m_pHRIR->getMaxEffectiveFilterLength( );
	int iLS2HeadDelaySamples = int( dDistanceMeters / m_oConfig.fSpeedOfSound * m_oConfig.fSampleRate );
	int iOffset              = ( std::max )( 0, ( int( iLS2HeadDelaySamples ) - iHRIRPreOffset - iDistanceCompensationSamples ) ); // remove starting zeros from HRIR

	// Check against buffer overrun, prevent if necessary and clamp to sweet spot boundary (end of IR)
	if( iOffset + iHRIRFilerTaps > sfDestHRIR.length( ) )
		iOffset = sfDestHRIR.length( ) - iHRIRFilerTaps;

	int iRecordIndex;
	m_pHRIR->getNearestNeighbour( DAFF_OBJECT_VIEW, float( dPhiDeg ), float( dThetaDeg ), iRecordIndex, bOutOfRange );
	m_pHRIR->addFilterCoeffs( iRecordIndex, 0, sfDestHRIR[0].data( ) + iOffset, fDistanceGain * float( dReflectionFactor ) );
	m_pHRIR->addFilterCoeffs( iRecordIndex, 1, sfDestHRIR[1].data( ) + iOffset, fDistanceGain * float( dReflectionFactor ) );

	return;
}

const ITANCTC::Pose& ITANCTC::GetLoudspeakerPose( const int iLoudspeakerID ) const
{
	if( iLoudspeakerID > GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Loudspeaker ID (starting from 1) out of range." );

	return m_oConfig.voLoudspeaker[iLoudspeakerID - 1].oPose;
}

void ITANCTC::CalculateFilter( std::vector<ITABase::CHDFTSpectra*>& vpCTCFilter ) const
{
	if( !m_pHRIR )
		ITA_EXCEPT1( MODAL_EXCEPTION, "CTC filters could not be created because HRIR is not set" );

	const float fMinDistance                  = GetMinimumDistanceHead2LS( );
	const int iMinDistanceCompensationSamples = int( fMinDistance / m_oConfig.fSpeedOfSound * m_oConfig.fSampleRate );

	bool bOutOfRange = false;
	for( int n = 0; n < GetNumChannels( ); n++ )
	{
		m_sfCTC_temp.zero( );

		switch( m_iOptimization )
		{
				// Here comes more if ready ... see feature/room_compensation branch
			case Config::OPTIMIZATION_NONE:
			default:
				AddHRIR( GetLoudspeakerPose( n + 1 ), m_sfCTC_temp, bOutOfRange, 1.0f, iMinDistanceCompensationSamples );
				break;
		}

		ITABase::CHDFTSpectra* pHRTF( m_vpHRTFs[n] );

		// Convert HRIRs to HRTFs
		m_fft.execute( m_sfCTC_temp[0].GetData( ), ( *pHRTF )[0]->GetData( ) );
		m_fft.execute( m_sfCTC_temp[1].GetData( ), ( *pHRTF )[1]->GetData( ) );

#ifdef NCTC_EXPORT_FILTER_TO_HARDDRIVE
		ITAFFTUtils::Export( pHRTF, "HRIR_LS" + IntToString( n + 1 ) + "_RAW" );
#endif // NCTC_EXPORT_FILTER_TO_HARDDRIVE
	}

	/* Matrix to be inverted (as two-by-two row vector)
	 * a  c
	 * b  d
	 */
	ITABase::CHDFTSpectrum* a = ( *m_vpHelper2x2[0] )[0];
	ITABase::CHDFTSpectrum* b = ( *m_vpHelper2x2[0] )[1];
	ITABase::CHDFTSpectrum* c = ( *m_vpHelper2x2[1] )[0];
	ITABase::CHDFTSpectrum* d = ( *m_vpHelper2x2[1] )[1];

	// Least-squares minimization: C = WH*(HWH*-\beta)^-1
	// using H* as the hermitian (complex conjugated) transpose of H

	/* Sweet spot optimization: modify input HRTFs
	 *   1. Crosstalk side will be lowered by CTC factor
	 *   2. HRTF will be flattened by WICK factor
	 */


	// Prepare HRTF spectra

	for( int n = 0; n < GetNumChannels( ); n++ )
	{
		ITABase::CHDFTSpectra* pHRTF( m_vpHRTFs[n] ); // two-channel


		//  --- WICK factor ---

		// First, store original energy of HRTF (left channel)
		const float fEnergyLeftChannel = ( *pHRTF )[0]->GetEnergy( );

		// Apply WICK factor only on magnitudes (left channel)
		for( int i = 0; i < ( *pHRTF )[0]->GetSize( ); i++ )
		{
			float fMag = ( *pHRTF )[0]->CalcMagnitude( i );
			( *pHRTF )[0]->SetMagnitudePreservePhase( i, std::pow( fMag, m_fWaveIncidenceAngleCompensationFactor ) );
		}

		// Compensate initial HRTF energy when WICK is used (left channel)
		assert( fEnergyLeftChannel > 0 );
		const float fEnergyCompensationLeftChannel = std::pow( fEnergyLeftChannel, ( 1 - m_fWaveIncidenceAngleCompensationFactor ) );
		( *pHRTF )[0]->Mul( fEnergyCompensationLeftChannel );


		// First, store original energy of HRTF only on magnitudes (right channel)
		const float fEnergyRightChannel = ( *pHRTF )[1]->GetEnergy( );

		// Apply WICK factor only on magnitude (right channel)
		for( int i = 0; i < ( *pHRTF )[1]->GetSize( ); i++ )
		{
			float fMag = ( *pHRTF )[1]->CalcMagnitude( i );
			( *pHRTF )[1]->SetMagnitudePreservePhase( i, std::pow( fMag, m_fWaveIncidenceAngleCompensationFactor ) );
		}

		// Compensate initial HRTF energy when WICK is used (right channel)
		assert( fEnergyRightChannel > 0 );
		const float fEnergyCompensationRightChannel = std::pow( fEnergyRightChannel, ( 1 - m_fWaveIncidenceAngleCompensationFactor ) );
		( *pHRTF )[1]->Mul( fEnergyCompensationRightChannel );


#ifdef NCTC_EXPORT_FILTER_TO_HARDDRIVE
		ITAFFTUtils::Export( pHRTF, "HRIR_LS" + IntToString( n + 1 ) + "_WICKed" );
#endif // NCTC_EXPORT_FILTER_TO_HARDDRIVE

		//  --- CTC compensation factor ---
		// CTC compensation factors
		const int iLSSide = GetLoudspeakerSide( n + 1 );

		float fRightChannelCTC = 1.0f;
		if( iLSSide == Config::Loudspeaker::LEFT_SIDE )
			fRightChannelCTC *= m_fCrossTalkCancellationFactor; // only apply to left channel if LS is at left side

		float fLeftChannelCTC = 1.0f;
		if( iLSSide == Config::Loudspeaker::RIGHT_SIDE )
			fLeftChannelCTC *= m_fCrossTalkCancellationFactor; // only apply to right channel if LS is at right side

		( *pHRTF )[0]->Mul( fLeftChannelCTC );
		( *pHRTF )[1]->Mul( fRightChannelCTC );

#ifdef NCTC_EXPORT_FILTER_TO_HARDDRIVE
		ITAFFTUtils::Export( pHRTF, "HRIR_LS" + IntToString( n + 1 ) + "_WICKedPlusCTCFactor" );
#endif // NCTC_EXPORT_FILTER_TO_HARDDRIVE

		//  --- Weighting ---
		const float& fWeight( m_vfWeights[n] ); // diag element

		// Element wise (a and d): HWH* -> 2x2
		t->Copy( ( *pHRTF )[0] );
		t->Mul( fWeight );

		t->MulConj( ( *pHRTF )[0] );
		n == 0 ? a->Copy( t ) : a->Add( t );

		t->Copy( ( *pHRTF )[1] );
		t->Mul( fWeight );

		t->MulConj( ( *pHRTF )[1] );
		n == 0 ? d->Copy( t ) : d->Add( t );

		// Cross elements (b and c): HWH*
		t->Copy( ( *pHRTF )[1] );
		t->Mul( fWeight );

		t->MulConj( ( *pHRTF )[0] );
		n == 0 ? b->Copy( t ) : b->Add( t );

		t->Copy( ( *pHRTF )[0] );
		t->Mul( fWeight );

		t->MulConj( ( *pHRTF )[1] );
		n == 0 ? c->Copy( t ) : c->Add( t );
	}


	// Regularize

	a->Add( m_fRegularizationFactor );
	d->Add( m_fRegularizationFactor );

	ITABase::CHDFTSpectra abcd( m_oConfig.fSampleRate, 4, m_oConfig.iCTCFilterLength );
	abcd[0]->CopyFrom( *a );
	abcd[1]->CopyFrom( *b );
	abcd[2]->CopyFrom( *c );
	abcd[3]->CopyFrom( *d );


	// Invert via determinant (simple 2x2 matrix inversion)
	/*
	 * d/det  -b/det
	 * -c/det  a/det
	 */
	det->Copy( a );
	det->Mul( d );
	t->Copy( b );
	t->Mul( c );
	det->Sub( t );

	t->Copy( d );
	d->Copy( a );
	a->Copy( t );
	b->Mul( -1.0f );
	c->Mul( -1.0f );
	a->Div( det );
	b->Div( det );
	c->Div( det );
	d->Div( det );


	// Calculate CTC filter WH*Inv (Nx2)

	ITASampleFrame sfTargetData_shift( m_sfCTC_temp.GetNumChannels( ), m_sfCTC_temp.GetLength( ), true );

	for( int n = 0; n < GetNumChannels( ); n++ )
	{
		ITABase::CHDFTSpectra* pHRTF( m_vpHRTFs[n] );        // two-channel, already WICKed
		ITABase::CHDFTSpectra* pCTCFilter( vpCTCFilter[n] ); // two-channel
		const float& fWeight( m_vfWeights[n] );              // diag element

		t->Copy( a );
		t->MulConj( ( *pHRTF )[0] );
		t->Mul( fWeight );
		( *pCTCFilter )[0]->Copy( t );

		t->Copy( b );
		t->MulConj( ( *pHRTF )[1] );
		t->Mul( fWeight );
		( *pCTCFilter )[0]->Add( t );

		t->Copy( c );
		t->MulConj( ( *pHRTF )[0] );
		t->Mul( fWeight );
		( *pCTCFilter )[1]->Copy( t );
		t->Copy( d );
		t->MulConj( ( *pHRTF )[1] );
		t->Mul( fWeight );
		( *pCTCFilter )[1]->Add( t );

#ifdef NCTC_EXPORT_FILTER_TO_HARDDRIVE
		ITAFFTUtils::Export( pCTCFilter, "CTCFilter_noshift_" + IntToString( n + 1 ) );
#endif // NCTC_EXPORT_FILTER_TO_HARDDRIVE


		// Time-shift
		m_ifft.execute( ( *pCTCFilter )[0]->GetData( ), m_sfCTC_temp[0].GetData( ) );
		m_ifft.execute( ( *pCTCFilter )[1]->GetData( ), m_sfCTC_temp[1].GetData( ) );

		// Normalize after IFFT
		m_sfCTC_temp.div_scalar( float( m_sfCTC_temp.GetLength( ) ) );

		// Shift the CTC filter according to desired delay
		int iShiftSamples = int( m_oConfig.fSampleRate * m_vfDelayTime[n] );
		if( m_vfDelayTime[n] < 0.0f )
			iShiftSamples = m_sfCTC_temp.GetLength( ) / 2; // if invalid, shift by half length
		sfTargetData_shift.cyclic_write( m_sfCTC_temp, m_sfCTC_temp.GetLength( ), 0, iShiftSamples );

		m_fft.execute( sfTargetData_shift[0].GetData( ), ( *pCTCFilter )[0]->GetData( ) );
		m_fft.execute( sfTargetData_shift[1].GetData( ), ( *pCTCFilter )[1]->GetData( ) );

#ifdef NCTC_EXPORT_FILTER_TO_HARDDRIVE
		ITAFFTUtils::Export( pCTCFilter, "CTCFilter_shift_" + IntToString( n + 1 ) );
#endif // NCTC_EXPORT_FILTER_TO_HARDDRIVE
	}
}

void ITANCTC::SetHRIR( const DAFFContentIR* pHRIR )
{
	// TODO: Set m_oConfig.iCTCFilterLength according to HRIR length and working area
	// TODO: Minimize loudspeaker delay

	if( m_pHRIR != pHRIR )
	{
		m_pHRIR = pHRIR;

		// Return if HRIR is reset (NULL)
		if( !m_pHRIR )
			return;

		if( m_pHRIR->getProperties( )->getNumberOfChannels( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "HRIR dataset must have exactly two channels" );

		// Get HRIR filter length
		if( m_pHRIR->getFilterLength( ) > m_oConfig.iCTCFilterLength )
			ITA_EXCEPT1( INVALID_PARAMETER, std::string( "The filter length of the HRIR database \"" ) + m_pHRIR->getParent( )->getFilename( ) +
			                                    std::string( "\" is larger than the CTC filters. This leads to buffer overruns." ) );

		// Get HRIR delay samples
		const DAFFMetadata* pHRIRMetadata = m_pHRIR->getParent( )->getMetadata( );

		if( !pHRIRMetadata->hasKey( "DELAY_SAMPLES" ) )
			ITA_EXCEPT1( INVALID_PARAMETER,
			             std::string( "HRIR database \"" ) + m_pHRIR->getParent( )->getFilename( ) + std::string( "\" is missing a \"DELAY_SAMPLES\" metadata tag" ) );
		if( ( pHRIRMetadata->getKeyType( "DELAY_SAMPLES" ) != DAFFMetadata::DAFF_INT ) && ( pHRIRMetadata->getKeyType( "DELAY_SAMPLES" ) != DAFFMetadata::DAFF_FLOAT ) )
			ITA_EXCEPT1( INVALID_PARAMETER, std::string( "The metadata tag \"DELAY_SAMPLES\" in HRIR database \"" ) + m_pHRIR->getParent( )->getFilename( ) +
			                                    std::string( "\" is not numeric" ) );

		float fHRIRDelay = (float)pHRIRMetadata->getKeyFloat( "DELAY_SAMPLES" );
		if( fHRIRDelay < 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, std::string( "The metadata tag \"DELAY_SAMPLES\" in HRIR database \"" ) + m_pHRIR->getParent( )->getFilename( ) +
			                                    std::string( "\" must not be negative" ) );
	}
}

void ITANCTC::SetRegularizationFactor( const float fRegularizationFactor )
{
	m_fRegularizationFactor = fRegularizationFactor;
}

float ITANCTC::GetRegularizationFactor( ) const
{
	return m_fRegularizationFactor;
}

void ITANCTC::SetCrossTalkCancellationFactor( const float fFactor )
{
	m_fCrossTalkCancellationFactor = fFactor;
}

float ITANCTC::GetCrossTalkCancellationFactor( ) const
{
	return m_fCrossTalkCancellationFactor;
}

void ITANCTC::SetWaveIncidenceAngleCompensationFactor( const float fFactor )
{
	m_fWaveIncidenceAngleCompensationFactor = fFactor;
}

float ITANCTC::GetWaveIncidenceAngleCompensation( ) const
{
	return m_fWaveIncidenceAngleCompensationFactor;
}

void ITANCTC::SetDelayTime( const float fDelayTime )
{
	for( int n = 0; n < GetNumChannels( ); n++ )
		m_vfDelayTime[n] = fDelayTime;
}

void ITANCTC::SetDelayTime( const std::vector<float>& vfDelayTime )
{
	if( int( vfDelayTime.size( ) ) != GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Provide as many delay values as channels for NCTC" );

	for( int n = 0; n < GetNumChannels( ); n++ )
		m_vfDelayTime[n] = vfDelayTime[n];
}

std::vector<float> ITANCTC::GetDelayTime( ) const
{
	std::vector<float> vfDelayTime( GetNumChannels( ) );
	for( int n = 0; n < GetNumChannels( ); n++ )
		vfDelayTime[n] = m_vfDelayTime[n];
	return vfDelayTime;
}

void ITANCTC::SetOptimization( const int iOptimization )
{
	m_iOptimization = iOptimization;
}

int ITANCTC::GetOptimization( ) const
{
	return m_iOptimization;
}

bool ITANCTC::GetHRTF( std::vector<ITABase::CHDFTSpectra*>& vpHRTF ) const
{
	if( m_vpHRTFs.empty( ) )
		return false;

	vpHRTF = m_vpHRTFs;
	return true;
}

ITANCTC::Pose ITANCTC::GetHeadPose( ) const
{
	return m_oHeadPose;
}

float ITANCTC::GetMinimumDistanceHead2LS( ) const
{
	assert( GetNumChannels( ) > 0 );
	float fMinDistance = ( m_oHeadPose.vPos - GetLoudspeakerPose( 1 ).vPos ).GetLength( );
	for( int n = 1; n < GetNumChannels( ); n++ )
	{
		if( ( m_oHeadPose.vPos - GetLoudspeakerPose( n + 1 ).vPos ).GetLength( ) < fMinDistance )
			fMinDistance = ( m_oHeadPose.vPos - GetLoudspeakerPose( n + 1 ).vPos ).GetLength( );
	}

	return fMinDistance;
}

// --- Loudspeaker ---

ITANCTC::Config::Loudspeaker::Loudspeaker( ) : pDirectivity( NULL ), iSide( SIDE_NOT_SPECIFIED ) {}

ITANCTC::Config::Loudspeaker::Loudspeaker( const Pose& oStaticPose ) : pDirectivity( NULL ), iSide( SIDE_NOT_SPECIFIED )
{
	oPose = oStaticPose;
}


// --- Pose ---

ITANCTC::Pose& ITANCTC::Pose::operator=( const ITANCTC::Pose& oPoseRHS )
{
	vPos = oPoseRHS.vPos;
	// qOrient = oPoseRHS.qOrient;
	vView = oPoseRHS.vView;
	vUp   = oPoseRHS.vUp;

	return *this;
}

void ITANCTC::Pose::SetOrientationYPRdeg( const float fYaw, const float fPitch, const float fRoll )
{
	const double yaw   = grad2rad( fYaw );
	const double pitch = grad2rad( fPitch );
	const double roll  = grad2rad( fRoll );

	const double sy = sin( yaw ), cy = cos( yaw );
	const double sp = sin( pitch ), cp = cos( pitch );
	const double sr = sin( roll ), cr = cos( roll );

	vView.SetValues( -sy * cp, sp, -cy * cp );
	vUp.SetValues( cy * sr + sy * sp * cr, cp * cr, -sy * sr + cy * sp * cr );

	return;
}

ITANCTC::Config::Config( )
{
	// Set some default values
	N                                     = 0;
	iCTCFilterLength                      = 4096;
	fSpeedOfSound                         = 344.0f;
	iOptimization                         = OPTIMIZATION_NONE;
	fCrossTalkCancellationFactor          = 1.0f;
	fWaveIncidenceAngleCompensationFactor = 1.0f;
	fRegularizationFactor                 = 0.0001f;
}
