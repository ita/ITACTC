// $Id: ITADualCTCStreamfilter.cpp,v 1.2 2008-12-01 13:31:22 fwefers Exp $

#include "ITADualCTCStreamfilter.h"

#include <DSMBCConvolver.h>
#include <DSMBCFilter.h>
#include <DSMBCFilterPool.h>
#include <FastMath.h>
#include <ITAException.h>
#include <ITAFunctors.h>
#include <ITAHPT.h>
#include <algorithm>

//#define FILTER_INDEX(SPEAKER,EAR,BUFFER)	(SPEAKER*4+EAR)*2+BUFFER

// Hilfsliterale f�r Filter-Indizes
#define FINDEX00 0
#define FINDEX10 1
#define FINDEX01 2
#define FINDEX11 3
//#define FINDEX02 4
//#define FINDEX12 5
//#define FINDEX03 6
//#define FINDEX13 7

ITADualCTCStreamfilter::ITADualCTCStreamfilter( double dSamplerate, int iBlocklength, int iFilterlength, int iFilterExchangeMode, int iFilterCrossfadeLength )
    : m_dSamplerate( dSamplerate )
    , m_iBlocklength( iBlocklength )
    , m_iFilterlength( iFilterlength )
    , m_pdsInput( NULL )
    , m_pdsOutput( NULL )
    , m_pFilterPool( NULL )
    , m_pFilterExchangeTrigger( new DSMBCTrigger )
    , m_vpConvolvers( 8, (DSMBCConvolver*)NULL )
{
	// Filterpool erzeugen (3 Filters�tze vorhalten)
	m_pFilterPool = new DSMBCFilterPool( m_iBlocklength, iFilterlength, 4 * 3 );

	// Falter und Filterstrukturen erzeugen (4 St�ck jeweils: 2 Lautsprecher * 2 Ohren)
	for( int i = 0; i < 4; i++ )
	{
		// Indizierung der Faltung: Eingang*4+Ausgang (Bsp: pfCTCFilter12 -> 4+2 = 6)
		m_vpConvolvers[i] = new DSMBCConvolver( m_iBlocklength, iFilterlength, m_pFilterPool );
		m_vpConvolvers[i]->setFilterExchangeMode( iFilterExchangeMode );
		m_vpConvolvers[i]->setFilterCrossfadeLength( iFilterCrossfadeLength );
	}

	m_pdsOutput = new ITADatasourceRealization( 2, m_dSamplerate, (unsigned int)m_iBlocklength );
	m_pdsOutput->SetStreamEventHandler( this );
}

ITADualCTCStreamfilter::~ITADualCTCStreamfilter( )
{
	// Hinweis: Erzeugte Filterstrukturen werden automatisch vom entsprechenden DSMBC freigegeben
	std::for_each( m_vpConvolvers.begin( ), m_vpConvolvers.end( ), deleteFunctor<DSMBCConvolver> );
	delete m_pFilterPool;
	delete m_pFilterExchangeTrigger;
	delete m_pdsOutput;

	printf( "[DualCTC] Convolution: min = %s, avg = %s, max = %s, std = %s\n", convertTimeToHumanReadableString( m_swConvolution.minimum( ) ).c_str( ),
	        convertTimeToHumanReadableString( m_swConvolution.mean( ) ).c_str( ), convertTimeToHumanReadableString( m_swConvolution.maximum( ) ).c_str( ),
	        convertTimeToHumanReadableString( m_swConvolution.std_deviation( ) ).c_str( ) );
	printf( "[DualCTC] Load filters: min = %s, avg = %s, max = %s, std = %s\n", convertTimeToHumanReadableString( m_swLoadFilters.minimum( ) ).c_str( ),
	        convertTimeToHumanReadableString( m_swLoadFilters.mean( ) ).c_str( ), convertTimeToHumanReadableString( m_swLoadFilters.maximum( ) ).c_str( ),
	        convertTimeToHumanReadableString( m_swLoadFilters.std_deviation( ) ).c_str( ) );
}

void ITADualCTCStreamfilter::setInputDatasource( ITADatasource* pdsInput )
{
	if( pdsInput )
	{
		// Parameter �berpr�fen
		if( pdsInput->GetSamplerate( ) != m_dSamplerate )
			ITA_EXCEPT1( INVALID_PARAMETER, "Datasource has different samplerate" );

		if( pdsInput->GetNumberOfChannels( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "Input datasource must be stereo" );

		if( pdsInput->GetBlocklength( ) != m_iBlocklength )
			ITA_EXCEPT1( INVALID_PARAMETER, "Datasource has different streaming blocklength" );
	}

	m_pdsInput = pdsInput;
}

ITADatasource* ITADualCTCStreamfilter::getOutputDatasource( ) const
{
	return m_pdsOutput;
}

void ITADualCTCStreamfilter::setFilter( int iIndex, const float* pfFilter )
{
	DSMBCFilter* pNewFilter = m_pFilterPool->requestFilter( );
	pNewFilter->load( pfFilter, m_iFilterlength );
	m_vpConvolvers[iIndex]->exchangeFilter( pNewFilter );
	pNewFilter->release( ); // Auto-release when out of use
}
void ITADualCTCStreamfilter::setFilters( const float* pfCTCFilter00, const float* pfCTCFilter10, const float* pfCTCFilter01, const float* pfCTCFilter11
                                         //					                    const float* pfCTCFilter02, const float* pfCTCFilter12,
                                         //										const float* pfCTCFilter03, const float* pfCTCFilter13
)
{
	m_swLoadFilters.start( );

	setFilter( FINDEX00, pfCTCFilter00 );
	setFilter( FINDEX10, pfCTCFilter10 );
	setFilter( FINDEX01, pfCTCFilter01 );
	setFilter( FINDEX11, pfCTCFilter11 );
	// setFilter(FINDEX02, pfCTCFilter02);
	// setFilter(FINDEX12, pfCTCFilter12);
	// setFilter(FINDEX03, pfCTCFilter03);
	// setFilter(FINDEX13, pfCTCFilter13);

	m_swLoadFilters.stop( );

	m_pFilterExchangeTrigger->trigger( );
}

void ITADualCTCStreamfilter::HandleProcessStream( ITADatasourceRealization* pSender, const ITAStreamInfo* pStreamInfo )
{
	const float* pfInputDataCh0 = m_pdsInput->GetBlockPointer( 0, pStreamInfo );
	const float* pfInputDataCh1 = m_pdsInput->GetBlockPointer( 1, pStreamInfo );

	float* pfOutputDataCh0 = m_pdsOutput->GetWritePointer( 0 );
	float* pfOutputDataCh1 = m_pdsOutput->GetWritePointer( 1 );
	//	float* pfOutputDataCh2 = m_pdsOutput->GetWritePointer(2);
	//	float* pfOutputDataCh3 = m_pdsOutput->GetWritePointer(3);

	m_swConvolution.start( );

	// Lautsprecher 1 (CTC-Filter00 und CTC-Filter10)
	m_vpConvolvers[FINDEX00]->process( pfInputDataCh0, m_iBlocklength, pfOutputDataCh0, m_iBlocklength, DSMBCConvolver::OUTPUT_OVERWRITE );
	m_vpConvolvers[FINDEX10]->process( pfInputDataCh1, m_iBlocklength, pfOutputDataCh0, m_iBlocklength, DSMBCConvolver::OUTPUT_MIX );

	// Lautsprecher 2 (CTC-Filter01 und CTC-Filter11)
	m_vpConvolvers[FINDEX01]->process( pfInputDataCh0, m_iBlocklength, pfOutputDataCh1, m_iBlocklength, DSMBCConvolver::OUTPUT_OVERWRITE );
	m_vpConvolvers[FINDEX11]->process( pfInputDataCh1, m_iBlocklength, pfOutputDataCh1, m_iBlocklength, DSMBCConvolver::OUTPUT_MIX );

	//	// Lautsprecher 3 (CTC-Filter02 und CTC-Filter12)
	//	m_vpConvolvers[FINDEX02]->process(pfInputDataCh0, m_iBlocklength, pfOutputDataCh2, m_iBlocklength, DSMBCConvolver::OUTPUT_OVERWRITE);
	//	m_vpConvolvers[FINDEX12]->process(pfInputDataCh1, m_iBlocklength, pfOutputDataCh2, m_iBlocklength, DSMBCConvolver::OUTPUT_MIX);
	//
	//	// Lautsprecher 4 (CTC-Filter03 und CTC-Filter13)
	//	m_vpConvolvers[FINDEX03]->process(pfInputDataCh0, m_iBlocklength, pfOutputDataCh3, m_iBlocklength, DSMBCConvolver::OUTPUT_OVERWRITE);
	//	m_vpConvolvers[FINDEX13]->process(pfInputDataCh1, m_iBlocklength, pfOutputDataCh3, m_iBlocklength, DSMBCConvolver::OUTPUT_MIX);

	m_swConvolution.stop( );

	m_pdsOutput->IncrementWritePointer( );
}

void ITADualCTCStreamfilter::HandlePostIncrementBlockPointer( ITADatasourceRealization* pSender )
{
	m_pdsInput->IncrementBlockPointer( );
}
