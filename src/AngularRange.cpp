// $Id: AngularRange.cpp,v 1.1 2008-09-10 22:38:11 fwefers Exp $

#include "AngularRange.h"

#include <ITAException.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <cmath>

AngularRange::AngularRange( ) : m_fLower( 0 ), m_fUpper( 0 ) {}

AngularRange::AngularRange( float fLower, float fUpper )
{
	setRange( fLower, fUpper );
}

float AngularRange::getLowerAngle( ) const
{
	return m_fLower;
}

float AngularRange::getUpperAngle( ) const
{
	return m_fUpper;
}

void AngularRange::setLowerAngle( float fLower )
{
	// F�r Sicherheitschecks usw. immer �ber die Methode "setRange" realisieren
	setRange( fLower, m_fUpper );
}

void AngularRange::setUpperAngle( float fUpper )
{
	// F�r Sicherheitschecks usw. immer �ber die Methode "setRange" realisieren
	setRange( m_fLower, fUpper );
}

void AngularRange::setRange( float fLower, float fUpper )
{
	/*
	 *  Falls die Winkeldifferenz gr��er gleich als 360� ist -> Vollst�ndiges Intervall
	 *
	 *  Hinweis: Dieser Schritt wird vor der Normierung durchgef�hrt damit z.B. -90�,530�
	 *           zu einem vollst�ndigen Intervall f�hrt
	 */

	if( fabs( fLower - fUpper ) >= 360.0 )
	{
		m_fLower = m_fUpper = 0;
		return;
	}

	// Nun die Winkel normieren, d.h. in das Interval [0�, 360�) abbilden
	m_fLower = anglef_proj_0_360_DEG( fLower );
	m_fUpper = anglef_proj_0_360_DEG( fUpper );
}

bool AngularRange::contains( float fAngle ) const
{
	float a = anglef_proj_0_360_DEG( fAngle );

	if( m_fLower <= m_fUpper )
		return ( a >= m_fLower ) && ( a <= m_fUpper );
	else
		return ( ( a >= m_fLower ) && ( a <= 360 ) ) || ( ( a <= m_fUpper ) && ( a >= 0 ) );
}

bool AngularRange::intersects( const AngularRange& dest ) const
{
	// Zwei Winkelbereiche �berlappen, falls ein Winkel des einen Intervalls im anderen Intervall enthalten ist
	return ( dest.contains( m_fLower ) || dest.contains( m_fUpper ) );
}

bool AngularRange::complete( ) const
{
	return m_fLower == m_fUpper;
}

std::string AngularRange::toString( ) const
{
	return std::string( "[" ) + FloatToString( m_fLower ) + std::string( "," ) + FloatToString( m_fUpper ) + std::string( "] deg" );
}
