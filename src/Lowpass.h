// $Id: Lowpass.h,v 1.1 2008-12-02 22:31:36 fwefers Exp $

#ifndef __LOWPASS_H__
#define __LOWPASS_H__

/*
 *  Mit Matlab vorberechnete, optimierte Tiefpassfilter(impulsantworten) f�r die
 *  Begrenzung der CTC-Filter am Ende des Frequenzbereiches >18 kHz. Dort f�hrt
 *  die Tiefpa�charakteristik der HRTFs (wg. Abtastung, Lautsprecher, Mikros)
 *  zu einer sehr hohen und falschen Kompensationsenergie. Diese Tiefp�sse werden
 *  dazu benutzt diese Energie zu entfernen. Sie ben�tigen sehr hohe Stopband-
 *  D�mpfungen von �ber 200 dB.
 */

// Tiefpass-Impulsantwort (equiripple, fS = 44100 Hz, fp = 14000.0 Hz, fst = 22000.0 Hz, ap = 0.1 dB, ast = 220.0 dB)
const int CTC_LOWPASS_LENGTH         = 20;
const float CTC_LOWPASS_RESPONSE[20] = { -0.000978996F, 0.008140337F,  -0.009034697F, 0.001463124F,  0.019481660F,  -0.046407113F, 0.059179053F,
	                                     -0.027741985F, -0.093754302F, 0.592134950F,  0.592134950F,  -0.093754302F, -0.027741985F, 0.059179053F,
	                                     -0.046407113F, 0.019481660F,  0.001463124F,  -0.009034697F, 0.008140337F,  -0.000978996F };


#endif // __LOWPASS_H__