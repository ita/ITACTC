// $Id: ITAQuadCTC.cpp,v 1.12 2008-12-02 22:31:36 fwefers Exp $

#include "ITAQuadCTC.h"

#include "Helpers.h"
#include "ITAQuadCTCStreamfilter.h"

#include <DSMBCConvolver.h>
#include <DSMBCFilter.h>
#include <FastMath.h>
#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITAConfigUtils.h>
#include <ITAConvolution.h>
#include <ITADebug.h>
#include <ITAException.h>
#include <ITAFilesystemUtils.h>
#include <ITAFilterDesign.h>
#include <ITAHPT.h>
#include <ITANumericUtils.h>
//#include <ITAStopwatch.h>
#include <DAFF.h>
#include <ITADatasource.h>
#include <ITAStringUtils.h>
#include <RG_LocalCS.h>

// STL-Includes
#include "Lowpass.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>

// Ordnung des fractional delay FIR-Filters
#define FD_ORDER 4

#define PI 3.14159265f

// Dateien schreiben
//#define WITH_DUMP

// Ausgaben auf die Konsole
//
//#define PRINT_INFOS

ITAQuadCTC::ITAQuadCTC( double dSamplerate, int iBlocklength, const std::string& sConfigurationFile, DAFFContentIR* pHRIR )
    : m_evHeadDataAvail( false )
    , m_bStopThread( false )
    ,

    m_dSamplerate( dSamplerate )
    , m_iBlocklength( iBlocklength )
    , m_pdsInput( NULL )
    , m_psfFilter( NULL )
    , m_pHRIRDatabase( pHRIR )
    , m_pHRIRReader( NULL )
    , m_viSpeakerChannels( 4, -1 )
    , m_viSpeakerStates( 4, 0 )
    ,

    m_pfHRIRBufCh0( NULL )
    , m_pfHRIRBufCh1( NULL )
    , m_pfFDResponse( NULL )
    , m_pfHRIR_LS0_Ch0( NULL )
    , m_pfHRIR_LS0_Ch1( NULL )
    , m_pfHRIR_LS1_Ch0( NULL )
    , m_pfHRIR_LS1_Ch1( NULL )
    , m_pfHRIR_LS2_Ch0( NULL )
    , m_pfHRIR_LS2_Ch1( NULL )
    , m_pfHRIR_LS3_Ch0( NULL )
    , m_pfHRIR_LS3_Ch1( NULL )
    , m_pfCTC_VLL( NULL )
    , m_pfCTC_VRR( NULL )
    , m_pfCTC_VLR( NULL )
    , m_pfCTC_VRL( NULL )
    , m_pfCTC_HLL( NULL )
    , m_pfCTC_HRR( NULL )
    , m_pfCTC_HLR( NULL )
    , m_pfCTC_HRL( NULL )
    ,

    m_pfTemp( NULL )
{
	SetThreadName( "QuadCTC::CalculationThread" );

	parseConfigurationFile( sConfigurationFile );

	// HRIR-Datensatz laden, falls in Konfigurationsdatei angegeben
	// und KEIN Datensatz �bergeben wurde
	if( !m_pHRIRDatabase )
	{
		if( m_sHRIRDataset.empty( ) )
			ITA_EXCEPT1( INVALID_PARAMETER, "No HRIR dataset provided" );

		m_pHRIRReader = DAFFReader::create( );
		int iResult   = m_pHRIRReader->openFile( m_sHRIRDataset );
		if( iResult != DAFF_NO_ERROR )
			ITA_EXCEPT1( IO_ERROR, DAFFUtils::StrError( iResult ) );

		// Pr�fen das Impulsantworten
		if( m_pHRIRReader->getContentType( ) != DAFF_IMPULSE_RESPONSE )
			ITA_EXCEPT1( INVALID_PARAMETER, std::string( "HRIR dataset file \"" ) + m_sHRIRDataset + std::string( "\" does not contain impulse responses" ) );

		m_pHRIRDatabase = dynamic_cast<DAFFContentIR*>( m_pHRIRReader->getContent( ) );
	}

	// HRIR Parameter �berpr�fen
	assert( m_pHRIRDatabase );

	if( m_pHRIRDatabase->getProperties( )->getNumberOfChannels( ) != 2 )
		ITA_EXCEPT1( INVALID_PARAMETER, "HRIR dataset must have exactly two channels" );

	m_iHRIRLength = m_pHRIRDatabase->getFilterLength( );

	const DAFFMetadata* pHRIRMetadata = m_pHRIRDatabase->getParent( )->getMetadata( );
	if( !pHRIRMetadata->hasKey( "DELAY_SAMPLES" ) )
		ITA_EXCEPT1( INVALID_PARAMETER, std::string( "HRIR database \"" ) + m_pHRIRDatabase->getParent( )->getFilename( ) +
		                                    std::string( "\" is missing a \"DELAY_SAMPLES\" metadata tag" ) );

	if( ( pHRIRMetadata->getKeyType( "DELAY_SAMPLES" ) != DAFFMetadata::DAFF_INT ) && ( pHRIRMetadata->getKeyType( "DELAY_SAMPLES" ) != DAFFMetadata::DAFF_FLOAT ) )
		ITA_EXCEPT1( INVALID_PARAMETER, std::string( "The metadata tag \"DELAY_SAMPLES\" in HRIR database \"" ) + m_pHRIRDatabase->getParent( )->getFilename( ) +
		                                    std::string( "\" is not numeric" ) );

	m_fHRIRDelay = (float)pHRIRMetadata->getKeyFloat( "DELAY_SAMPLES" );
	if( m_fHRIRDelay < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, std::string( "The metadata tag \"DELAY_SAMPLES\" in HRIR database \"" ) + m_pHRIRDatabase->getParent( )->getFilename( ) +
		                                    std::string( "\" must not be negative" ) );

	// TODO:
	m_iCTCFilterLength = 1024;

	// HRIR-Puffer erzeugen
	m_pfHRIRBufCh0 = fm_falloc( m_pHRIRDatabase->getFilterLength( ) );
	m_pfHRIRBufCh1 = fm_falloc( m_pHRIRDatabase->getFilterLength( ) );

	m_pfHRIR_LS0_Ch0 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS0_Ch1 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS1_Ch0 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS1_Ch1 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS2_Ch0 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS2_Ch1 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS3_Ch0 = fm_falloc( m_iCTCFilterLength );
	m_pfHRIR_LS3_Ch1 = fm_falloc( m_iCTCFilterLength );

	m_pfCTC_VLL = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_VRR = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_VLR = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_VRL = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_HLL = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_HRR = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_HLR = fm_falloc( m_iCTCFilterLength );
	m_pfCTC_HRL = fm_falloc( m_iCTCFilterLength );

	CTC_Filter90LS0_0  = fm_falloc( m_iCTCFilterLength );
	CTC_Filter90LS0_1  = fm_falloc( m_iCTCFilterLength );
	CTC_Filter90LS1_0  = fm_falloc( m_iCTCFilterLength );
	CTC_Filter90LS1_1  = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LS0_0 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LS0_1 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LS1_0 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LS1_1 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LSH_0 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LSH_1 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LSV_0 = fm_falloc( m_iCTCFilterLength );
	CTC_Filter180LSV_1 = fm_falloc( m_iCTCFilterLength );

	m_pfTemp = fm_falloc( m_iCTCFilterLength );


	m_spCTC_LL.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spCTC_LR.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spCTC_RL.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spCTC_RR.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spH_LL.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spH_RR.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spH_LR.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spH_RL.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	m_spK.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );

	m_spLowpass.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );


	// F�r die Filterberechnung
	determinante.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	a.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	b.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	c.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	d.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	temp.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	HLLcon.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	HRLcon.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	HLRcon.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );
	HRRcon.init( m_iCTCFilterLength ).setSamplerate( m_dSamplerate );

	// FFT/IFFT prototypisch planen
	m_fft.plan( ITAFFT::FFT_R2C, m_iCTCFilterLength, m_pfHRIR_LS0_Ch1, m_spCTC_LL.data( ) );
	m_ifft.plan( ITAFFT::IFFT_C2R, m_iCTCFilterLength, m_spCTC_LL.data( ), m_pfCTC_VLL );

	// Tiefpass ins den Frequenzbereich transformieren (benutzt tempor�r den Puffer der HRIR)
	fm_zero( m_pfTemp, m_iCTCFilterLength );
	memcpy( m_pfTemp, CTC_LOWPASS_RESPONSE, CTC_LOWPASS_LENGTH * sizeof( float ) );
	m_fft.execute( m_pfTemp, m_spLowpass.data( ) );

	// Streamfilter erzeugen
	m_psfFilter = new ITAQuadCTCStreamfilter( dSamplerate, iBlocklength, m_iCTCFilterLength, DSMBCConvolver::CROSSFADE_LINEAR, iBlocklength >> 2 );

	// TODO: Genaue Allozierung. Momentan gro�z�gig.
	m_iFDSize      = FD_ORDER + 1;
	m_pfFDResponse = fm_falloc( m_iFDSize );

	// Voreinstellungen
	m_bUseFractionalDelays = false;
	m_iOrder               = 6;

	// Verz�gerungen
	m_iLowpassDelay = uprdiv( CTC_LOWPASS_LENGTH, 2 );
	// m_iMeasurementDistanceDelay = (int) ceil(m_pHRIRDatabase->GetMeasurementDistance() / m_fc * m_pHRIRDatabase->GetSamplingrate());
	// m_iFlexDelay = (int) ceil( (m_fL2SDistMax - m_fL2SDistMin) / m_fc * m_pHRIRDatabase->GetSamplingrate() );
	m_iFlexDelay = (int)ceil( m_fL2SDistMax / m_fc * m_pHRIRDatabase->getSamplerate( ) );
	m_iOnset     = 0;

	// Filterbechnungs-Thread starten
	m_evHeadDataAvail.ResetThisEvent( );

	// Queue vorreservieren
	m_vHeadData.reserve( 16 );

	// Beim ersten Update muss Neuberechnet werden
	m_bForceFilterUpdate = true;

	if( !Run( ) )
		ITA_EXCEPT1( UNKNOWN, "Failed to start CTC filter calculation thread" );
}

ITAQuadCTC::~ITAQuadCTC( )
{
	// Filterberechnungs-Thread ordentlich beenden
	m_bStopThread = true;
	m_evHeadDataAvail.SignalEvent( );
	StopGently( true );


	DEBUG_PRINTF( "[QuadCTC] Calculation time: Min %s, Avg %s, Max %s, Std %s\n", convertTimeToHumanReadableString( m_swCTCFilterCalc.minimum( ) ).c_str( ),
	              convertTimeToHumanReadableString( m_swCTCFilterCalc.mean( ) ).c_str( ), convertTimeToHumanReadableString( m_swCTCFilterCalc.maximum( ) ).c_str( ),
	              convertTimeToHumanReadableString( m_swCTCFilterCalc.std_deviation( ) ).c_str( ) );

	delete m_psfFilter;
	// delete m_pSpeakerDirectivity;
	// delete m_pHRIRDatabase;

	fm_free( m_pfHRIRBufCh0 );
	fm_free( m_pfHRIRBufCh1 );
	fm_free( m_pfFDResponse );

	fm_free( m_pfHRIR_LS0_Ch0 );
	fm_free( m_pfHRIR_LS0_Ch1 );
	fm_free( m_pfHRIR_LS1_Ch0 );
	fm_free( m_pfHRIR_LS1_Ch1 );
	fm_free( m_pfHRIR_LS2_Ch0 );
	fm_free( m_pfHRIR_LS2_Ch1 );
	fm_free( m_pfHRIR_LS3_Ch0 );
	fm_free( m_pfHRIR_LS3_Ch1 );

	fm_free( m_pfCTC_VLL );
	fm_free( m_pfCTC_VRR );
	fm_free( m_pfCTC_VLR );
	fm_free( m_pfCTC_VRL );
	fm_free( m_pfCTC_HLL );
	fm_free( m_pfCTC_HRR );
	fm_free( m_pfCTC_HLR );
	fm_free( m_pfCTC_HRL );

	fm_free( m_pfTemp );

	// HRIR-Datensatz freigeben, falls geladen
	delete m_pHRIRReader;
}

void ITAQuadCTC::setInputDatasource( ITADatasource* pdsInput )
{
	m_psfFilter->setInputDatasource( pdsInput );
}

ITADatasource* ITAQuadCTC::getOutputDatasource( ) const
{
	return m_psfFilter->getOutputDatasource( );
}

void ITAQuadCTC::getPhysicalOutputChannels( std::vector<int>& viChannels ) const
{
	viChannels.clear( );
	std::copy( m_viSpeakerChannels.begin( ), m_viSpeakerChannels.end( ), viChannels.begin( ) );
}

void ITAQuadCTC::testLS2H( RG_Vector& vPos )
{
	float phi, theta, length;

	for( int i = 0; i < 4; i++ )
	{
		m_csSpeaker[i].getAnglesToTarget( vPos, phi, theta, length );
#ifdef PRINT_INFOS
		DEBUG_PRINTF( "LS%d->Kopf: Azimuth %3.1f�, Elevation %3.1f�, Distanz %0.3fm\n", i + 1, correctAngle180( phi ), correctAngle180( theta ), length );
#endif
	}
}

double ITAQuadCTC::getLatency( )
{
	return m_dLatency;
}

/*
void ITAQuadCTC::updateHeadPosition(const RG_Vector& vPos, const RG_Vector& vView, const RG_Vector& vUp, bool bForceFilterUpdate) {
    if (bForceFilterUpdate || mustRecalculateFilters(vPos, vView, vUp)) {
        // Neue Kopfposition setzen
        m_headpos.vPos  = vPos;
        m_headpos.vView = vView;
        m_headpos.vUp   = vUp;

        // Filter neuberechnen
        calculateFilters();

        //determineConfig();
    }
}
*/

void ITAQuadCTC::updateHeadPosition( const RG_Vector& vPos, const RG_Vector& vView, const RG_Vector& vUp, bool bForceFilterUpdate )
{
	m_mxHeadData.Lock( );
	m_vHeadData.push_back( PositionViewUp( vPos, vView, vUp ) );
	m_bForceFilterUpdate |= bForceFilterUpdate;
	m_evHeadDataAvail.SignalEvent( );
	m_mxHeadData.Unlock( );
}

void ITAQuadCTC::updateHeadPosition( double px, double py, double pz, double vx, double vy, double vz, double ux, double uy, double uz, bool bForceFilterUpdate )
{
	RG_Vector vPos( (float)px, (float)py, (float)pz );
	RG_Vector vView( (float)vx, (float)vy, (float)vz );
	RG_Vector vUp( (float)ux, (float)uy, (float)uz );
	updateHeadPosition( vPos, vView, vUp, bForceFilterUpdate );
}

void ITAQuadCTC::updateHeadPosition( const float fX, const float fY, const float fZ, const float fPhiDEG, const float fThetaDEG, const float fRhoDEG,
                                     bool bForceFilterUpdate )
{
	RG_Vector vPos( fX, fY, fZ ), vView, vUp;

	// View- und Up-Vektor aus der Rotationsangabe berechnen
	Angles_to_VUVec( grad2radf( fPhiDEG ), grad2radf( fThetaDEG ), grad2radf( fRhoDEG ), vView, vUp );

	updateHeadPosition( vPos, vView, vUp );
}

bool ITAQuadCTC::mustRecalculateFilters( const RG_Vector& vPos, const RG_Vector& vView, const RG_Vector& vUp )
{
	// DEBUG: Immer Updaten
	// return true;

	// Einfache Metrik: Bewegung des Kopfes
	RG_Vector v = m_headpos.vPos;
	v -= vPos;

	// 1 mm
	return ( v.length( ) > 0.001 );
}

void ITAQuadCTC::calculateFilterSet( float* HRIR_LS0_Ch0, float* HRIR_LS0_Ch1, float* HRIR_LS1_Ch0, float* HRIR_LS1_Ch1, float* CTC_FilterLS0_0, float* CTC_FilterLS0_1,
                                     float* CTC_FilterLS1_0, float* CTC_FilterLS1_1 )
{
	float* m_pfHRIR_LL = HRIR_LS0_Ch0;
	float* m_pfHRIR_LR = HRIR_LS0_Ch1;
	float* m_pfHRIR_RL = HRIR_LS1_Ch0;
	float* m_pfHRIR_RR = HRIR_LS1_Ch1;


	float* m_pfCTC_LL = CTC_FilterLS0_0;
	float* m_pfCTC_RL = CTC_FilterLS0_1;
	float* m_pfCTC_LR = CTC_FilterLS1_0;
	float* m_pfCTC_RR = CTC_FilterLS1_1;


	// HRIRs in den Frequenzbereich transformieren
	m_fft.execute( m_pfHRIR_LL, m_spH_LL.data( ) );
	m_fft.execute( m_pfHRIR_RR, m_spH_RR.data( ) );
	m_fft.execute( m_pfHRIR_LR, m_spH_LR.data( ) );
	m_fft.execute( m_pfHRIR_RL, m_spH_RL.data( ) );

	//-------------------------------------------------------
	// alternativer ansatz durch regularisierung
	//-------------------------------------------------------
	/*  beta = 0.0000001;
	 //   % [a b; c d] = H'.H + beta*I
	 //   a = HLL*conj(HLL) + HLR*conj(HLR) + beta;
	 //   b = HRL*conj(HLL) + HRR*conj(HLR);
	 //   c = HLL*conj(HRL) + HLR*conj(HRR);
	 //   d = HRL*conj(HRL) + HRR*conj(HRR) + beta;
	 //   determinant = a*d - b*c;
	 //
	 //   % [LL RL; LR RR] = inv(H'.H +beta) H'
	 //   CTC_LL = (d*conj(HLL) - b*conj(HRL))/determinant;
	 //   CTC_LR = (a*conj(HRL) - c*conj(HLL))/determinant;
	 //   CTC_RL = (d*conj(HLR) - b*conj(HRR))/determinant;
	 //   CTC_RR = (a*conj(HRR) - c*conj(HLR))/determinant;


	*/
#ifdef WITH_DUMP
	dumpSpectrumText( "m_spH_LL.txt", m_spH_LL );
#endif

	// float betafaktor=0.1;
	float betafaktor = m_fBeta;
	// Berechnung der Matrixkoeffizienten

	HLLcon.copyFrom( m_spH_LL );
	HRLcon.copyFrom( m_spH_RL );
	HLRcon.copyFrom( m_spH_LR );
	HRRcon.copyFrom( m_spH_RR );
	HLLcon.conjugate( );
	HRLcon.conjugate( );
	HLRcon.conjugate( );
	HRRcon.conjugate( );


	//   % [a b; c d] = H'.H + beta*I
	//   a = HLL*conj(HLL) + HLR*conj(HLR) + beta;

	a.copyFrom( m_spH_LL );

	a.mul( HLLcon );
	temp.copyFrom( m_spH_LR );
	temp.mul( HLRcon );
	a.add( temp );
	a.add( betafaktor );


	//   b = HRL*conj(HLL) + HRR*conj(HLR);
	b.copyFrom( m_spH_RL );
	b.mul( HLLcon );
	temp.copyFrom( m_spH_RR );
	temp.mul( HLRcon );
	b.add( temp );


	//   c = HLL*conj(HRL) + HLR*conj(HRR);
	c.copyFrom( m_spH_LL );
	c.mul( HRLcon );
	temp.copyFrom( m_spH_LR );
	temp.mul( HRRcon );
	c.add( temp );

	//   d = HRL*conj(HRL) + HRR*conj(HRR) + beta;
	d.copyFrom( m_spH_RL );
	d.mul( HRLcon );
	temp.copyFrom( m_spH_RR );
	temp.mul( HRRcon );
	d.add( temp );
	d.add( betafaktor );

	//   determinant = a*d - b*c;
	determinante.copyFrom( a );
	determinante.mul( d );
	temp.copyFrom( b );
	temp.mul( c );
	determinante.sub( temp );

	//   % [LL RL; LR RR] = inv(H'.H +beta) H'
	//   CTC_LL = (d*conj(HLL) - b*conj(HRL))/determinant;
	m_spCTC_LL.copyFrom( d );
	m_spCTC_LL.mul( HLLcon );
	temp.copyFrom( b );
	temp.mul( HRLcon );
	m_spCTC_LL.sub( temp );
	m_spCTC_LL.div( determinante );

	//   CTC_LR = (a*conj(HRL) - c*conj(HLL))/determinant;
	m_spCTC_LR.copyFrom( a );
	m_spCTC_LR.mul( HRLcon );
	temp.copyFrom( c );
	temp.mul( HLLcon );
	m_spCTC_LR.sub( temp );
	m_spCTC_LR.div( determinante );

	//   CTC_RL = (d*conj(HLR) - b*conj(HRR))/determinant;
	m_spCTC_RL.copyFrom( d );
	m_spCTC_RL.mul( HLRcon );
	temp.copyFrom( b );
	temp.mul( HRRcon );
	m_spCTC_RL.sub( temp );
	m_spCTC_RL.div( determinante );

	//   CTC_RR = (a*conj(HRR) - c*conj(HLR))/determinant;
	m_spCTC_RR.copyFrom( a );
	m_spCTC_RR.mul( HRRcon );
	temp.copyFrom( c );
	temp.mul( HLRcon );
	m_spCTC_RR.sub( temp );
	m_spCTC_RR.div( determinante );


	int start = (int)( 16000 / ( m_dSamplerate / m_iCTCFilterLength ) );
	int ende  = (int)( 20000 / ( m_dSamplerate / m_iCTCFilterLength ) );

	/*for (int i=0;i<=512;i++) {
	        m_spCTC_LL.data()[2*i]=1.0;
	        m_spCTC_LL.data()[2*i+1]=0;
	}*/


	/*for (int i=0;i<=5;i++) { //LL tief
	    float a = (float) i;
	    float betrag = m_spCTC_LL.data()[2*i]*m_spCTC_LL.data()[2*i]+m_spCTC_LL.data()[2*i+1]*m_spCTC_LL.data()[2*i+1];
	    betrag=sqrt(betrag);

	    float faktorb=1 - (a-5)*(a-5)*(1-1/betrag)/25;

	    m_spCTC_LL.data()[2*i]=m_spCTC_LL.data()[2*i]*faktorb;
	    m_spCTC_LL.data()[2*i+1]=faktorb*m_spCTC_LL.data()[2*i+1];

	}
	for (int i=0;i<=5;i++) { // RR tief
	    float a = (float) i;
	    float betrag=m_spCTC_RR.data()[2*i]*m_spCTC_RR.data()[2*i]+m_spCTC_RR.data()[2*i+1]*m_spCTC_RR.data()[2*i+1];
	    betrag=sqrt(betrag);

	float faktorb=1 - (a-5)*(a-5)*(1-1/betrag)/25;

	    m_spCTC_RR.data()[2*i]=m_spCTC_RR.data()[2*i]*faktorb;
	    m_spCTC_RR.data()[2*i+1]=faktorb*m_spCTC_RR.data()[2*i+1];

	}
	for (int i=0;i<=5;i++) { // RL tief
	    float a = (float) i;
	    float betrag = m_spCTC_RL.data()[2*i]*m_spCTC_RL.data()[2*i]+m_spCTC_RL.data()[2*i+1]*m_spCTC_RL.data()[2*i+1];
	    betrag = sqrt(betrag);

	    float faktorb=1 - (a-5)*(a-5)*(1-1/betrag)/25;

	    m_spCTC_RL.data()[2*i]=m_spCTC_RL.data()[2*i]*faktorb;
	    m_spCTC_RL.data()[2*i+1]=faktorb*m_spCTC_RL.data()[2*i+1];

	}
	for (int i=0;i<=5;i++)//LR tief
	{
	    float a = (float) i;
	    float betrag=m_spCTC_LR.data()[2*i]*m_spCTC_LR.data()[2*i]+m_spCTC_LR.data()[2*i+1]*m_spCTC_LR.data()[2*i+1];
	    betrag=sqrt(betrag);

	    float faktorb=1 - (a-5)*(a-5)*(1-1/betrag)/25;

	    m_spCTC_LR.data()[2*i]=m_spCTC_LR.data()[2*i]*faktorb;
	    m_spCTC_LR.data()[2*i+1]=faktorb*m_spCTC_LR.data()[2*i+1];

	}*/

	/*for (int i=start;i<=511;i++) { //LL hoch
	    if (i>ende) {
	        m_spCTC_LL.data()[2*i]=0;
	        m_spCTC_LL.data()[2*i+1]=0;

	    } else {


	        float faktorb=1-(float) (i-start)*(i-start)/((ende-start)*(ende-start));



	    m_spCTC_LL.data()[2*i]=m_spCTC_LL.data()[2*i]*faktorb;
	        m_spCTC_LL.data()[2*i+1]=faktorb*m_spCTC_LL.data()[2*i+1];
	    }

	}
	//dumpSpectrum("test.dat",m_spCTC_LL);
	//dumpSpectrumText("test.txt",m_spCTC_LL);






	for (int i=start;i<=511;i++) { // RL hoch
	    if (i>ende) {
	        m_spCTC_RL.data()[2*i]=0;
	        m_spCTC_RL.data()[2*i+1]=0;

	    } else {

	        float faktorb=1-(float) (i-start)*(i-start)/((ende-start)*(ende-start));

	        m_spCTC_RL.data()[2*i]=m_spCTC_RL.data()[2*i]*faktorb;
	        m_spCTC_RL.data()[2*i+1]=faktorb*m_spCTC_RL.data()[2*i+1];
	    }

	}


	//RR hoch
	for (int i=start;i<=511;i++) {
	    if (i>ende) {
	        m_spCTC_RR.data()[2*i]=0;
	        m_spCTC_RR.data()[2*i+1]=0;
	    } else {

	        float faktorb=1-(float) (i-start)*(i-start)/((ende-start)*(ende-start));

	        m_spCTC_RR.data()[2*i]=m_spCTC_RR.data()[2*i]*faktorb;
	        m_spCTC_RR.data()[2*i+1]=faktorb*m_spCTC_RR.data()[2*i+1];
	    }

	}


	for (int i=start;i<=511;i++) { // LR hoch
	    if (i>ende) {
	        m_spCTC_LR.data()[2*i]=0;
	        m_spCTC_LR.data()[2*i+1]=0;

	    } else {

	        float faktorb=1-(float) (i-start)*(i-start)/((ende-start)*(ende-start));

	        m_spCTC_LR.data()[2*i]=m_spCTC_LR.data()[2*i]*faktorb;
	        m_spCTC_LR.data()[2*i+1]=faktorb*m_spCTC_LR.data()[2*i+1];
	    }
	}*/


	/*
	 *  R�cktransformation in den Zeitbereich und Normalisierung
	 */

	float fNorm = 1 / (float)m_iCTCFilterLength;
	fm_mul( m_spCTC_LL.data( ), fNorm, m_spCTC_LL.getSize( ) * 2 );
	fm_mul( m_spCTC_RR.data( ), fNorm, m_spCTC_RR.getSize( ) * 2 );
	fm_mul( m_spCTC_LR.data( ), fNorm, m_spCTC_LR.getSize( ) * 2 );
	fm_mul( m_spCTC_RL.data( ), fNorm, m_spCTC_RL.getSize( ) * 2 );


	/*
	 *  CTC-Filter in den Zeitbereich zur�cktransformieren und dabei
	 *  die zeitliche Anordnung der CTC-Filter korregieren (durch zyklisches Shiften)
	 */
	// int iCompDelay = m_iLowpassDelay + m_iFlexDelay + m_iOnset;
	int iCompDelay = m_iFlexDelay;
	// iCompDelay=0;
	int rc = m_iCTCFilterLength - iCompDelay; // Lesecursor im Quellsignal
	int j;
	if( rc < 0 )
		ITA_EXCEPT1( UNKNOWN, "Delay compensation < 0" );

	m_ifft.execute( m_spCTC_LL.data( ), m_pfTemp );
	j = rc;
	for( int i = 0; i < m_iCTCFilterLength; i++ )
	{
		m_pfCTC_LL[i] = m_pfTemp[j++];
		j %= m_iCTCFilterLength;
	}

	m_ifft.execute( m_spCTC_LR.data( ), m_pfTemp );
	j = rc;
	for( int i = 0; i < m_iCTCFilterLength; i++ )
	{
		m_pfCTC_LR[i] = m_pfTemp[j++];
		j %= m_iCTCFilterLength;
	}

	m_ifft.execute( m_spCTC_RL.data( ), m_pfTemp );
	j = rc;
	for( int i = 0; i < m_iCTCFilterLength; i++ )
	{
		m_pfCTC_RL[i] = m_pfTemp[j++];
		j %= m_iCTCFilterLength;
	}

	m_ifft.execute( m_spCTC_RR.data( ), m_pfTemp );
	j = rc;
	for( int i = 0; i < m_iCTCFilterLength; i++ )
	{
		m_pfCTC_RR[i] = m_pfTemp[j++];
		j %= m_iCTCFilterLength;
	}

	double dRuntime = m_swCTCFilterCalc.stop( );

#ifdef PRINT_INFOS
	DEBUG_PRINTF( "Calculation took %s\n", convertTimeToHumanReadableString( dRuntime ).c_str( ) );
#endif


#ifdef WITH_DUMP
	dumpFilters( "CTC-Filters.wav", m_pfCTC_LL, m_pfCTC_LR, m_pfCTC_RL, m_pfCTC_RR, m_iCTCFilterLength );
#endif
}


void ITAQuadCTC::calculateFilters( )
{
#ifdef PRINT_INFOS
	DEBUG_PRINTF( "[ITAQuadCTC::calculateFilters]\n" );
	DEBUG_PRINTF(
	    "Head position: P = (%0.4fm, %0.4fm, %0.4fm)\n"
	    "               V = (%0.4fm, %0.4fm, %0.4fm)\n"
	    "               U = (%0.4fm, %0.4fm, %0.4fm)\n",
	    m_headpos.vPos[0], m_headpos.vPos[1], m_headpos.vPos[2], m_headpos.vView[0], m_headpos.vView[1], m_headpos.vView[2], m_headpos.vUp[0], m_headpos.vUp[1],
	    m_headpos.vUp[2] );
#endif

	m_swCTCFilterCalc.start( );

	/*
	 *  Winkel + Entfernungen vom Kopf zu allen LS berechnen
	 *
	 *  Hinweis: Der View-Vektor des jeweiligen Lautsprechers muss invertiert
	 *           werden, damit er nicht "zum Kopf" zeigt. Multiplikation mit -1
	 */

	bool bOutOfRange;
	float pfPhiLS[4], pfThetaLS[4], pfDistanceLS[4];

	m_csHead.init( m_headpos.vPos, m_headpos.vView, m_headpos.vUp );

	for( int i = 0; i < 4; i++ )
	{
		m_csHead.getAnglesToTarget( m_csSpeaker[i].getPosition( ), pfPhiLS[i], pfThetaLS[i], pfDistanceLS[i] );
#ifdef PRINT_INFOS
		DEBUG_PRINTF( "Kopf->LS%d: Azimuth %3.1f�, Elevation %3.1f�, Distanz %0.3fm\n", i + 1, correctAngle180( pfPhiLS[i] ), correctAngle180( pfThetaLS[i] ),
		              pfDistanceLS[i] );
#endif
	}

	/*
	// Distanzen Kopf->Lautsprecher bestimmen
	float fDist[4];
	for (int i=0; i<4; i++)
	    fDist[i] = RG_Vector(m_headpos.vPos, m_csSpeaker[i].getPosition()).length();

	DEBUG_PRINTF("Distances head to LS1 = %0.4fm, LS2 = %0.4fm, LS3 = %0.4fm, LS4 = %0.4fm\n",
	             fDist[0], fDist[1], fDist[2], fDist[3]);
	*/

	// Signal-Laufzeiten (signal runtime) Lautsprecher->Kopf berechnen [s],
	// sowie als Verz�gerungen in Samples (nicht-ganzzahlig) und getrennt
	// nach Ganzzahl-Verz�gerung und Gebrochener-Verz�gerung (fractional delay) berechnen
	float fSRT[4];
	for( int i = 0; i < 4; i++ )
		fSRT[i] = pfDistanceLS[i] / m_fc;

	float fDelay[4];
	float fFractionalDelay[4];
	int iIntegerDelay[4];

	for( int i = 0; i < 4; i++ )
	{
		fDelay[i]           = fSRT[i] * (float)m_dSamplerate;
		iIntegerDelay[i]    = (int)floor( fDelay[i] );
		fFractionalDelay[i] = fDelay[i] - iIntegerDelay[i];
	}

	/*
	DEBUG_PRINTF("Delays head to LS1 = %0.1fms (%0.1f samples), LS2 = %0.1fms (%0.1f samples), LS3 = %0.1fms (%0.1f samples), LS4 = %0.1fms (%0.1f samples)\n",
	             fSRT[0]*1000, fDelay[0],
	             fSRT[1]*1000, fDelay[1],
	             fSRT[2]*1000, fDelay[2],
	             fSRT[3]*1000, fDelay[3]);
	*/
#ifdef PRINT_INFOS
	for( int i = 0; i < 4; i++ )
		DEBUG_PRINTF( "Kopf->LS%d: A=%3.1f deg, E=%3.1f deg, r=%0.3fm (= %0.3f ms = %0.2f samples)\n", i + 1, correctAngle180( pfPhiLS[i] ),
		              correctAngle180( pfThetaLS[i] ), pfDistanceLS[i], fSRT[i] * 1000, fDelay[i] );
#endif

	/*
	 *  Vierkanal-CTC mit den Lautsprechern 1, 2, 3 und 4
	 */


	getHRIR( pfPhiLS[0], pfThetaLS[0], pfDistanceLS[0], m_pfHRIR_LS0_Ch0, m_pfHRIR_LS0_Ch1, bOutOfRange );
	if( bOutOfRange )
	{
		DEBUG_PRINTF( "[ITACTC] Problem: Head->LS1 out of coverage HRIR directions\n" );
		return;
	}
	getHRIR( pfPhiLS[1], pfThetaLS[1], pfDistanceLS[1], m_pfHRIR_LS1_Ch0, m_pfHRIR_LS1_Ch1, bOutOfRange );
	if( bOutOfRange )
	{
		DEBUG_PRINTF( "[ITACTC] Problem: Head->LS2 out of coverage HRIR directions\n" );
		return;
	}
	getHRIR( pfPhiLS[2], pfThetaLS[2], pfDistanceLS[2], m_pfHRIR_LS2_Ch0, m_pfHRIR_LS2_Ch1, bOutOfRange );
	if( bOutOfRange )
	{
		DEBUG_PRINTF( "[ITACTC] Problem: Head->LS3 out of coverage HRIR directions\n" );
		return;
	}
	getHRIR( pfPhiLS[3], pfThetaLS[3], pfDistanceLS[3], m_pfHRIR_LS3_Ch0, m_pfHRIR_LS3_Ch1, bOutOfRange );
	if( bOutOfRange )
	{
		DEBUG_PRINTF( "[ITACTC] Problem: Head->LS4 out of coverage HRIR directions\n" );
		return;
	}


	std::vector<std::pair<int, float> > LSConfig;
	LSConfig = determineConfig( );

	float* vorne0;
	float* vorne1;
	float* rechts0;
	float* rechts1;
	float* links0;
	float* links1;
	float* hinten0;
	float* hinten1;


	// Je nach ausrichtung des Kopfes werden die HRTFs zu den Lautsprechern relativ zum Kopf geordnet.
	// Der Lautsprecher mit dem kleinsten Winkel zum Kopf entspricht demnach vorne

	switch( LSConfig[0].first )
	{
		case 0:
			links0  = m_pfHRIR_LS3_Ch0;
			links1  = m_pfHRIR_LS3_Ch1;
			vorne0  = m_pfHRIR_LS0_Ch0;
			vorne1  = m_pfHRIR_LS0_Ch1;
			rechts0 = m_pfHRIR_LS1_Ch0;
			rechts1 = m_pfHRIR_LS1_Ch1;
			hinten0 = m_pfHRIR_LS2_Ch0;
			hinten1 = m_pfHRIR_LS2_Ch1;

			break;
		case 1:
			links0  = m_pfHRIR_LS0_Ch0;
			links1  = m_pfHRIR_LS0_Ch1;
			vorne0  = m_pfHRIR_LS1_Ch0;
			vorne1  = m_pfHRIR_LS1_Ch1;
			rechts0 = m_pfHRIR_LS2_Ch0;
			rechts1 = m_pfHRIR_LS2_Ch1;
			hinten0 = m_pfHRIR_LS3_Ch0;
			hinten1 = m_pfHRIR_LS3_Ch1;

			break;
		case 2:
			links0  = m_pfHRIR_LS1_Ch0;
			links1  = m_pfHRIR_LS1_Ch1;
			vorne0  = m_pfHRIR_LS2_Ch0;
			vorne1  = m_pfHRIR_LS2_Ch1;
			rechts0 = m_pfHRIR_LS3_Ch0;
			rechts1 = m_pfHRIR_LS3_Ch1;
			hinten0 = m_pfHRIR_LS0_Ch0;
			hinten1 = m_pfHRIR_LS0_Ch1;

			break;
		case 3:
			links0  = m_pfHRIR_LS2_Ch0;
			links1  = m_pfHRIR_LS2_Ch1;
			vorne0  = m_pfHRIR_LS3_Ch0;
			vorne1  = m_pfHRIR_LS3_Ch1;
			rechts0 = m_pfHRIR_LS0_Ch0;
			rechts1 = m_pfHRIR_LS0_Ch1;
			hinten0 = m_pfHRIR_LS1_Ch0;
			hinten1 = m_pfHRIR_LS1_Ch1;
			break;
	}


	// Berechnung von den beiden 180�-Konfigurationen.

	calculateFilterSet( links0, links1, rechts0, rechts1, CTC_Filter180LS0_0, CTC_Filter180LS0_1, CTC_Filter180LS1_0, CTC_Filter180LS1_1 );
	calculateFilterSet( vorne0, vorne1, hinten0, hinten1, CTC_Filter180LSV_0, CTC_Filter180LSV_1, CTC_Filter180LSH_0, CTC_Filter180LSH_1 );


	// Berechnung der 90� Filterkonfiguration. Das ist entweder vorne mit rechts oder links mit vorne, je nach Winkel Kopf->Lautsprecher
	if( ( LSConfig[0].first - LSConfig[1].first + 4 ) % 4 == 3 )
		calculateFilterSet( vorne0, vorne1, rechts0, rechts1, CTC_Filter90LS0_0, CTC_Filter90LS0_1, CTC_Filter90LS1_0, CTC_Filter90LS1_1 );

	else if( ( LSConfig[0].first - LSConfig[1].first + 4 ) % 4 == 1 )
		calculateFilterSet( links0, links1, vorne0, vorne1, CTC_Filter90LS0_0, CTC_Filter90LS0_1, CTC_Filter90LS1_0, CTC_Filter90LS1_1 );


	// Fading: Eingabe der beiden 180�-Konfigurationen und der 90� Konfiguration, zus�tzlich LSConfig (enth�lt Kopf->Lautsprecher-Anordnung) und Fadefunktion
	SektorFading( CTC_Filter180LS0_0, CTC_Filter180LS0_1, CTC_Filter180LS1_0, CTC_Filter180LS1_1, CTC_Filter90LS0_0, CTC_Filter90LS0_1, CTC_Filter90LS1_0,
	              CTC_Filter90LS1_1, CTC_Filter180LSV_0, CTC_Filter180LSV_1, CTC_Filter180LSH_0, CTC_Filter180LSH_1, LSConfig, m_iFadingMode );


	// Neuberechnete Filter in den Falter geben
	m_psfFilter->setFilters( m_pfCTC_VLL, m_pfCTC_VLR, m_pfCTC_VRL, m_pfCTC_VRR, m_pfCTC_HRL, m_pfCTC_HRR, m_pfCTC_HLL, m_pfCTC_HLR );

	// dumpFilters("CTC.wav",m_pfCTC_VLL,m_pfCTC_VLR,m_pfCTC_VRL,m_pfCTC_VRR,m_iCTCFilterLength);
	// writeAudiofile("HRIR1.wav",m_pfHRIR_LS1_Ch0, m_pfHRIR_LS1_Ch1,m_iCTCFilterLength,m_dSamplerate);
	// writeAudiofile("HRIR0.wav",m_pfHRIR_LS0_Ch0, m_pfHRIR_LS0_Ch1,m_iCTCFilterLength,m_dSamplerate);
}

void ITAQuadCTC::SektorFading( float* Filter180_LL, float* Filter180_LR, float* Filter180_RL, float* Filter180_RR, float* Filter90_LL, float* Filter90_LR,
                               float* Filter90_RL, float* Filter90_RR, float* Filter180_VL, float* Filter180_VR, float* Filter180_HL, float* Filter180_HR,
                               std::vector<std::pair<int, float> > LSConfiguration, int Fading )
{
	float *Filtervorne0, *Filtervorne1, *Filterrechts0, *Filterrechts1, *Filterlinks0, *Filterlinks1, *Filterhinten0, *Filterhinten1;

	// R�ckindizierung der relativen Lausprecherkonfiguration auf die globalen Filter
	switch( LSConfiguration[0].first )
	{
		case 0:
			Filtervorne0  = m_pfCTC_VLL;
			Filtervorne1  = m_pfCTC_VLR;
			Filterrechts0 = m_pfCTC_VRL;
			Filterrechts1 = m_pfCTC_VRR;
			Filterlinks0  = m_pfCTC_HLL;
			Filterlinks1  = m_pfCTC_HLR;
			Filterhinten0 = m_pfCTC_HRL;
			Filterhinten1 = m_pfCTC_HRR;

			break;
		case 1:
			Filterhinten0 = m_pfCTC_HLL;
			Filterhinten1 = m_pfCTC_HLR;
			Filtervorne0  = m_pfCTC_VRL;
			Filtervorne1  = m_pfCTC_VRR;
			Filterrechts0 = m_pfCTC_HRL;
			Filterrechts1 = m_pfCTC_HRR;
			Filterlinks0  = m_pfCTC_VLL;
			Filterlinks1  = m_pfCTC_VLR;

			break;
		case 2:
			Filterlinks0  = m_pfCTC_VRL;
			Filterlinks1  = m_pfCTC_VRR;
			Filterhinten0 = m_pfCTC_VLL;
			Filterhinten1 = m_pfCTC_VLR;
			Filtervorne0  = m_pfCTC_HRL;
			Filtervorne1  = m_pfCTC_HRR;
			Filterrechts0 = m_pfCTC_HLL;
			Filterrechts1 = m_pfCTC_HLR;

			break;
		case 3:
			Filterrechts0 = m_pfCTC_VLL;
			Filterrechts1 = m_pfCTC_VLR;
			Filterlinks0  = m_pfCTC_HRL;
			Filterlinks1  = m_pfCTC_HRR;
			Filterhinten0 = m_pfCTC_VRL;
			Filterhinten1 = m_pfCTC_VRR;
			Filtervorne0  = m_pfCTC_HLL;
			Filtervorne1  = m_pfCTC_HLR;

			break;
	}


	// bestimmt, in welchem Anteil des Bereichs zwischen 2 Konfigs (90� und 180�) gefadet wird. Momentan 33%
	// float threshold = 0.33333f;
	float range90180fade = 30;

	// bestimmt die Fadingrange zwischen 2 180� Konfigs. Momentan 30�
	float range180fade = 30;

	// Hilfsvariablen
	// float fade90;
	// float fade180;
	float sinusfade;
	//�berblendfaktoren
	float FadeFaktor;
	float minusFade;


	// Berechnung der Winkel zwischen der LS-Konfiguration und dem Blickwinkel vom Kopf
	float fPhiBisec01 = LSConfiguration[0].second + anglef_mindiff_0_360_DEG( LSConfiguration[0].second, LSConfiguration[1].second ) / 2; // 90�
	float fPhiBisec21 = LSConfiguration[2].second + anglef_mindiff_0_360_DEG( LSConfiguration[2].second, LSConfiguration[1].second ) / 2; // 180�
	float fPhiBisec03 = LSConfiguration[0].second + anglef_mindiff_0_360_DEG( LSConfiguration[0].second, LSConfiguration[3].second ) / 2; // 180� abgewandt
	// DEBUG_PRINTF("A: %3.3f ;B: %3.3f ;C: %3.3f ; %i %i \n",LSConfiguration[2].second,LSConfiguration[1].second,anglef_mindiff_0_360_DEG(LSConfiguration[2].second,
	// LSConfiguration[1].second),LSConfiguration[2].first,LSConfiguration[1].first);


	// Korrektur, da anglef_mindiff_0_360_DEG bei Winkeln nahe 180� nicht richtig rundet.
	// Zus�tzlich Korrektur f�r 180�Konfig auf das Intervall [-90�;+90�]
	if( fPhiBisec21 <= -180 )
		fPhiBisec21 += 180;
	else if( fPhiBisec21 >= 180 )
		fPhiBisec21 -= 180;
	if( fPhiBisec21 <= -90 )
		fPhiBisec21 += 180;
	else if( fPhiBisec21 >= 90 )
		fPhiBisec21 -= 180;


	if( fPhiBisec03 <= -180 )
		fPhiBisec03 += 180;
	else if( fPhiBisec03 >= 180 )
		fPhiBisec03 -= 180;
	if( fPhiBisec03 <= -90 )
		fPhiBisec03 += 180;
	else if( fPhiBisec03 >= 90 )
		fPhiBisec03 -= 180;


	// Winkelbereich den 90� und 180� Konfig aufspannen. range=45 wenn LS im Quadrat und Kopf in der Mitte
	float range = abs( fPhiBisec21 - fPhiBisec01 );
	// Winkelbereich beider 180� Konfigs
	float range180 = abs( fPhiBisec21 - fPhiBisec03 );

	// Vorbewertung: wenn ausserhalb der Fading-range, dann kein Fading
	float abstand90180 = abs( fPhiBisec21 + fPhiBisec01 );

	if( ( abstand90180 > range90180fade ) && ( ( Fading == 1 || Fading == 2 ) ) ) // f�r 90�/180�-Fading
		Fading = 0;

	float abstand = abs( fPhiBisec21 + fPhiBisec03 ) / 2;
	// DEBUG_PRINTF("EINS: %3.3f ; ZWEI: %3.3f ; Abstand: %3.3f\n",fPhiBisec21,fPhiBisec03,abstand);
	if( ( abstand > range180fade ) && Fading == 5 ) // 180� und 180�
	{
		Fading = 4;
	}


	switch( Fading )
	{
		case 0: // kein Fading, hartes Umschalten zwischen 90� und 180� Konfig

			if( abs( fPhiBisec01 ) <= abs( fPhiBisec21 ) ) // 90�
			{
				FadeFaktor = 1;
			}
			else // 180�
			{
				FadeFaktor = 0;
			}

			break;
		case 1: // Lineares Fading zwischen 90� und 180� Konfig
			if( abs( fPhiBisec01 ) <= abs( fPhiBisec21 ) )
				FadeFaktor = abstand90180 / ( range90180fade * 2 ) + 0.5f;
			else
				FadeFaktor = -abstand90180 / ( range90180fade * 2 ) + 0.5f;

			/*fade90=abs(fPhiBisec01)-threshold*range;
			fade180=abs(fPhiBisec21)-threshold*range;
			FadeFaktor=fade180/(fade90+fade180);*/
			// DEBUG_PRINTF("abstand: %3.1f ; verh: %3.3f ; Fadefaktor: %1.3f \n", abstand90180,abstand90180/(range90180fade),FadeFaktor);

			break;
		case 2: // Sinusoides Fading zwischen 90� und 180� Konfig
			if( abs( fPhiBisec01 ) <= abs( fPhiBisec21 ) )
				sinusfade = abstand90180 * PI / ( range90180fade * 2 ) + 0.5f;
			else
				sinusfade = -abstand90180 * PI / ( range90180fade * 2 ) + 0.5f;

			FadeFaktor = ( -cos( sinusfade ) + 1 ) * 0.5f;
			break;
		case 3: // immer 90� Konfig!
			FadeFaktor = 1;
			break;
		case 4: // immer 180� Konfig!
			FadeFaktor = 0;
			break;
		case 5: // Fading zwischen 2 180� Konfigurationen.
			// am rand (abstand=range180) ist Fadefaktor 0 =>kein Fading
			// genau zwischen 2 Konfigs (abstand=0) ist Fadefaktor=0.5 =>beide Konfigs gleichstark
			FadeFaktor = -abstand / ( range180fade * 2 ) + 0.5f;
			// DEBUG_PRINTF("Abstand: %3.3f; FadeFaktor: %3.3f;\n",abstand,FadeFaktor);
			break;
	}

	// Filter wieder zuweisen und gewichten

	// Energie Quadratisch zu Amplitude, deswegen Wurzel
	minusFade  = sqrt( 1 - FadeFaktor );
	FadeFaktor = sqrt( FadeFaktor );

	// vorne mit rechts
	// rechter Lautsprecher bekommt Superposition aus beiden Konfigs
	if( LSConfiguration[0].second >= 0 && Fading != 5 )
		for( int i = 0; i < m_iCTCFilterLength; i++ )
		{
			Filtervorne0[i] = Filter90_LL[i] * FadeFaktor;
			Filtervorne1[i] = Filter90_LR[i] * FadeFaktor;

			Filterlinks0[i] = Filter180_LL[i] * minusFade;
			Filterlinks1[i] = Filter180_LR[i] * minusFade;

			Filterrechts0[i] = Filter180_RL[i] * minusFade * minusFade + Filter90_RL[i] * FadeFaktor * FadeFaktor;
			Filterrechts1[i] = Filter180_RR[i] * minusFade * minusFade + Filter90_RR[i] * FadeFaktor * FadeFaktor;

			Filterhinten0[i] = 0;
			Filterhinten1[i] = 0;
		}
	// vorne mit links
	// linker Lautsprecher bekommt Superposition aus beiden Konfigs
	else if( LSConfiguration[0].second < 0 && Fading != 5 )
		for( int i = 0; i < m_iCTCFilterLength; i++ )
		{
			Filtervorne0[i] = Filter90_RL[i] * FadeFaktor;
			Filtervorne1[i] = Filter90_RR[i] * FadeFaktor;

			Filterlinks0[i] = Filter180_LL[i] * minusFade * minusFade + Filter90_LL[i] * FadeFaktor * FadeFaktor;
			Filterlinks1[i] = Filter180_LR[i] * minusFade * minusFade + Filter90_LR[i] * FadeFaktor * FadeFaktor;

			Filterrechts0[i] = Filter180_RL[i] * minusFade;
			Filterrechts1[i] = Filter180_RR[i] * minusFade;

			Filterhinten0[i] = 0;
			Filterhinten1[i] = 0;
		}
	// 180� Fading, hier keine Superposition!
	else if( Fading == 5 )
		for( int i = 0; i < m_iCTCFilterLength; i++ )
		{
			Filterlinks0[i]  = Filter180_LL[i] * minusFade;
			Filterlinks1[i]  = Filter180_LR[i] * minusFade;
			Filterrechts0[i] = Filter180_RL[i] * minusFade;
			Filterrechts1[i] = Filter180_RR[i] * minusFade;
			Filtervorne0[i]  = Filter180_VL[i] * FadeFaktor;
			Filtervorne1[i]  = Filter180_VR[i] * FadeFaktor;
			Filterhinten0[i] = Filter180_HL[i] * FadeFaktor;
			Filterhinten1[i] = Filter180_HR[i] * FadeFaktor;
		}
}


void ITAQuadCTC::getLoudspeakerPositionOrientation( int iSpeaker, RG_Vector& vPos, RG_Vector& vView, RG_Vector& vUp ) const
{
	assert( ( iSpeaker >= 0 ) && ( iSpeaker < 4 ) );
	vPos  = m_csSpeaker[iSpeaker].getPosition( );
	vView = m_csSpeaker[iSpeaker].getViewDirection( );
	vUp   = m_csSpeaker[iSpeaker].getUpDirection( );
}

void ITAQuadCTC::getLoudspeakerStates( std::vector<int>& viStates )
{
	viStates    = m_viSpeakerStates;
	viStates[1] = 1;
}


// void ITAQuadCTC::getS2HAngles(int iSpeaker, const RG_Vector& vPos, float& fAzimuth, float& fElevation);

void ITAQuadCTC::getHRIR( double dAzimuthDEG, double dElevationDEG, double dDistanceMeters, float* pfDataCh0, float* pfDataCh1, bool& bOutOfRange )
{
	// Einf�geposition der HRIR bestimmen
	float fDelay = (float)dDistanceMeters / m_fc * (float)m_dSamplerate;
	int iOffset  = (int)( fDelay - m_fHRIRDelay );

	if( iOffset < 0 )
	{
		// Fail-safe: Positiven Offset, auch wenn falsche Impulsantwort
		iOffset = 0;
		DEBUG_PRINTF( "[ITACTC] HRIR underruns CTC filter range\n" );
	}

	if( ( iOffset + m_iHRIRLength ) > m_iCTCFilterLength )
	{
		// Fail-safe: Maximalen Offset begrenzen, damit kein Buffer-Overrun auftritt
		iOffset = m_iCTCFilterLength - m_iHRIRLength;
		DEBUG_PRINTF( "[ITACTC] HRIR overruns CTC filter range\n" );
	}

	// 1/r Verst�rkungsfaktor f�r Distanz
	// Hinweis: Die Aussteuerung der HRIR-Datenbank ist nach Konvention auf 1 m normiert.
	float fDistanceGain = 1 / (float)dDistanceMeters;

	fm_zero( pfDataCh0, m_iCTCFilterLength );
	fm_zero( pfDataCh1, m_iCTCFilterLength );

	int iRecordIndex;
	m_pHRIRDatabase->getNearestNeighbour( DAFF_OBJECT_VIEW, (float)dAzimuthDEG, (float)dElevationDEG, iRecordIndex, bOutOfRange );
	m_pHRIRDatabase->getFilterCoeffs( iRecordIndex, 0, pfDataCh0 + iOffset, fDistanceGain );
	m_pHRIRDatabase->getFilterCoeffs( iRecordIndex, 1, pfDataCh1 + iOffset, fDistanceGain );

	/*
	double dDelay = (dDistanceMeters - dMeasurementDistance) / m_fc * m_dSamplerate;
	int iDelay = (int) floor(dDelay);	// Integral delay
	dDelay -= iDelay;					// Fractional delay

	int iInsertCh0 = iDelay + (int) uiOffsetCh0;
	int iInsertCh1 = iDelay + (int) uiOffsetCh1;
	int iNegativeDelay0=0;
	int iNegativeDelay1=0;
	    if (iInsertCh0<0){
	    iNegativeDelay0=-iInsertCh0;
	    iInsertCh0=0;
	}
	if (iInsertCh1<0){
	    iNegativeDelay1=-iInsertCh1;
	    iInsertCh1=0;
	}


	// Sicherheitscheck
	if ((iInsertCh0 < 0) || (iInsertCh1 < 0)) {
	    DEBUG_PRINTF("CTC-Panic! Negative insertion positions! Debug me, baby!\n");
	    return;
	}

	// Sicherheitcheck: Au�erhalb des abgedeckten HRIR-Bereiches -> Abbruch! (Filter sind Null)
	if (bOutOfRange) {
	    DEBUG_PRINTF("CTC-Panic! Outside HRIR range! Debug me, baby!\n");
	    return;
	}

	// Entfernung als Verst�rkung einrechnen
	double dg = getDistanceGain(dDistanceMeters);
	for (int i=0; i<m_pHRIRDatabase->getFilterLength(); i++)
	    m_pfHRIRBufCh0[i] *= (float) dg;
	for (int i=0; i<m_pHRIRDatabase->getFilterLength(); i++)
	    m_pfHRIRBufCh1[i] *= (float) dg;


	// Fractional delays


	//m_bUseFractionalDelays = false;
	if (m_bUseFractionalDelays) {
	    // Fractional delays draufrechnen (FD-Filter entwerfen + Faltung im Zeitbereich)
	    // Momentan: Lagrange-Methode 4. Ordnung

	    fd_filter_lagrange(m_pfFDResponse, FD_ORDER, dDelay);
	    /* TODO: Wieder einkommentieren und conv-Funktion verf�gbar machen
	    conv(m_pfHRIRBufCh0, m_pHRIRDatabase->GetRTFSize(),
	         m_pfFDResponse, m_iFDSize,
	         pfDataCh0 + iInsertCh0, m_iCTCFilterLength - iInsertCh0);

	    conv(m_pfHRIRBufCh1, m_pHRIRDatabase->GetRTFSize(),
	         m_pfFDResponse, m_iFDSize,
	         pfDataCh1 + iInsertCh1, m_iCTCFilterLength - iInsertCh1);

	} else {
	    // Keine fractional delays



	    memcpy(pfDataCh0 + iInsertCh0, m_pfHRIRBufCh0+iNegativeDelay0, (m_pHRIRDatabase->getFilterLength()-iNegativeDelay0-iInsertCh0) * sizeof(float));
	    memcpy(pfDataCh1 + iInsertCh1, m_pfHRIRBufCh1+iNegativeDelay1, (m_pHRIRDatabase->getFilterLength()-iNegativeDelay1-iInsertCh1) * sizeof(float));

	}
	*/
}

void ITAQuadCTC::testHRIR( )
{
	float* A = new float[m_iCTCFilterLength];
	float* B = new float[m_iCTCFilterLength];
	bool b;

	getHRIR( 0, 0, 2.00, A, B, b );

	try
	{
		// writeAudiofile("HRIR.wav", A, B, m_iCTCFilterLength, this->GetSamplerate(), ITA_INT24);
		writeAudiofile( "HRIR.wav", A, m_iCTCFilterLength, m_dSamplerate, ITA_INT24 );
	}
	catch( ... )
	{
		delete[] A;
		delete[] B;
		throw;
	}

	delete[] A;
	delete[] B;
}

// double ITAQuadCTC::getDistanceGain(double dDistance) {
//	/*
//	 *  Annahme Punktschallquelle im Freifeld: p ~ 1/r
//	 *
//	 *  p(r) := r_0 / r * p_0
//	 *
//	 *  mit  r_0 Messabstand der Datenbank (Konvention: 1m)
//	 *       p_0 Bezugspegel bei Messabstand
//	 *
//	 */
//
//	// TODO stienen: this is a hack, to do it right get the distance from the HRIR file
//	double dMeasurementDistance = 1;
//
//	return dMeasurementDistance / dDistance;
//	// return m_pHRIRDatabase->GetMeasurementDistance() / dDistance;
//}

void ITAQuadCTC::parseConfigurationFile( const std::string& sConfigurationFile )
{
	// +------------------------------+
	// |  Konfigurationsdatei parsen  |
	// +------------------------------+

	if( !doesFileExist( sConfigurationFile ) )
		ITA_EXCEPT1( FILE_NOT_FOUND, "CTC configuration file \"" + sConfigurationFile + "\" not found" );

	std::vector<float> v;

	INIFileUseFile( sConfigurationFile );

	// -= Globale Parameter =-
	INIFileUseSection( "Global" );

	// Schallgeschwindigkeit
	m_fc = INIFileReadFloatExplicit( "c" );
	if( m_fc <= 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\", section [Global]: Invalid value for \"c\"" );

	// Beta f�r Regularisierung
	m_fBeta = INIFileReadFloatExplicit( "beta" );
	if( m_fBeta <= 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\", section [Global]: Invalid value for \"beta\"" );

	// Fadingfunktion
	m_iFadingMode = INIFileReadIntExplicit( "fading" );
	if( ( m_iFadingMode <= 0 ) || ( m_iFadingMode > 5 ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\", section [Global]: Invalid value for \"fading\"" );


	// Arbeitsbereich
	if( INIFileKeyExists( "XRange" ) )
	{
		v = INIFileReadFloatList( "XRange" );
		if( v.size( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [XRange] must have the format MIN_VALUE, MAX_VALUE" );
		m_bounds.fXMin = ( std::min )( v[0], v[1] );
		m_bounds.fXMax = ( std::max )( v[0], v[1] );

		v = INIFileReadFloatList( "YRange" );
		if( v.size( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [YRange] must have the format MIN_VALUE, MAX_VALUE" );
		m_bounds.fYMin = ( std::min )( v[0], v[1] );
		m_bounds.fYMax = ( std::max )( v[0], v[1] );

		v = INIFileReadFloatList( "ZRange" );
		if( v.size( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [ZRange] must have the format MIN_VALUE, MAX_VALUE" );
		m_bounds.fZMin = ( std::min )( v[0], v[1] );
		m_bounds.fZMax = ( std::max )( v[0], v[1] );
	}

	// Minimal-/Maximalabstand zu den Lautsprechern
	m_fL2SDistMin = INIFileReadFloatExplicit( "L2SDistMin" );
	m_fL2SDistMax = INIFileReadFloatExplicit( "L2SDistMax" );

	if( m_fL2SDistMin < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [L2SDistMin] must not be negative" );

	if( m_fL2SDistMax < m_fL2SDistMin )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [L2SDistMax] must not be smaller than the value of [L2SDistMin]" );

	m_sHRIRDataset = INIFileReadString( "HRIRDataset" );

	// -= H�rer =-

	INIFileUseSection( "Listener" );

	v = INIFileReadFloatListExplicit( "LOffset" );
	if( v.size( ) != 3 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [LOffset] has invalid format" );

	m_pvEarOffset[0].init( v[0], v[1], v[2] );

	v = INIFileReadFloatListExplicit( "ROffset" );
	if( v.size( ) != 3 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sConfigurationFile + "\": Key [ROffset] has invalid format" );

	m_pvEarOffset[1].init( v[0], v[1], v[2] );

	// -= Lautsprecher =-
	parseSpeakerSection( 0, "Speaker1", sConfigurationFile );
	parseSpeakerSection( 1, "Speaker2", sConfigurationFile );
	parseSpeakerSection( 2, "Speaker3", sConfigurationFile );
	parseSpeakerSection( 3, "Speaker4", sConfigurationFile );

	// -= Sektoren =-
	INIFileUseSection( "Sectors" );

	parseSector( "Sector12Span", sConfigurationFile, m_arSector12Span );
	parseSector( "Sector12Core", sConfigurationFile, m_arSector12Core );
	parseSector( "Sector23Span", sConfigurationFile, m_arSector23Span );
	parseSector( "Sector23Core", sConfigurationFile, m_arSector23Core );
	parseSector( "Sector34Span", sConfigurationFile, m_arSector34Span );
	parseSector( "Sector34Core", sConfigurationFile, m_arSector34Core );
	parseSector( "Sector41Span", sConfigurationFile, m_arSector41Span );
	parseSector( "Sector41Core", sConfigurationFile, m_arSector41Core );

	parseSector( "Sector13Span", sConfigurationFile, m_arSector13Span );
	parseSector( "Sector13Core", sConfigurationFile, m_arSector13Core );
	parseSector( "Sector24Span", sConfigurationFile, m_arSector24Span );
	parseSector( "Sector24Core", sConfigurationFile, m_arSector24Core );
	parseSector( "Sector31Span", sConfigurationFile, m_arSector31Span );
	parseSector( "Sector31Core", sConfigurationFile, m_arSector31Core );
	parseSector( "Sector42Span", sConfigurationFile, m_arSector42Span );
	parseSector( "Sector42Core", sConfigurationFile, m_arSector42Core );

#ifdef PRINT_INFOS
	DEBUG_PRINTF( "C1 Sector spans: 12 = %s, 23 = %s, 34 = %s, 41 = %s\n", m_arSector12Span.toString( ).c_str( ), m_arSector23Span.toString( ).c_str( ),
	              m_arSector34Span.toString( ).c_str( ), m_arSector41Span.toString( ).c_str( ) );
	DEBUG_PRINTF( "C1 Sector cores: 12 = %s, 23 = %s, 34 = %s, 41 = %s\n", m_arSector12Core.toString( ).c_str( ), m_arSector23Core.toString( ).c_str( ),
	              m_arSector34Core.toString( ).c_str( ), m_arSector41Core.toString( ).c_str( ) );
#endif
}

void ITAQuadCTC::parseSpeakerSection( int iSpeaker, const std::string& sName, const std::string& sParentConfigurationFile )
{
	INIFileUseSection( sName );

	std::vector<float> x;
	RG_Vector vPos, vView, vUp;

	// Position
	x = INIFileReadFloatListExplicit( "Position" );
	vPos.init( x[0], x[1], x[2] );

	// Rotation
	x = INIFileReadFloatListExplicit( "Direction" );

	// View- und Up-Vektor aus der Rotationsangabe berechnen
	Angles_to_VUVec( grad2radf( x[0] ), grad2radf( x[1] ), grad2radf( x[2] ), vView, vUp );

	// Lokales Koordinatensystem des Lautsprechers initialisieren
	m_csSpeaker[iSpeaker].init( vPos, vView, vUp );

	// Ausgabekanal
	m_viSpeakerChannels[iSpeaker] = INIFileReadIntExplicit( "Channel" ) - 1;

	// DEBUG:
#ifdef PRINT_INFOS
	DEBUG_PRINTF(
	    "Loudspeaker %d: P = (%0.4fm, %0.4fm, %0.4fm)\n"
	    "               V = (%0.4fm, %0.4fm, %0.4fm)\n"
	    "               U = (%0.4fm, %0.4fm, %0.4fm)\n\n",
	    iSpeaker + 1, vPos[0], vPos[1], vPos[2], vView[0], vView[1], vView[2], vUp[0], vUp[1], vUp[2] );
#endif
}

void ITAQuadCTC::parseSector( const std::string& sName, const std::string& sParentConfigurationFile, AngularRange& arange )
{
	std::vector<float> v = INIFileReadFloatListExplicit( sName, '-' );
	if( v.size( ) != 2 )
		ITA_EXCEPT1( INVALID_PARAMETER, "In configuration file \"" + sParentConfigurationFile + "\": Key [" + sName + "] has invalid format" );
	arange.setRange( v[0], v[1] );
}

void ITAQuadCTC::dumpFilters( const std::string& sFilename, float* pf1, float* pf2, float* pf3, float* pf4, int iLength )
{
	ITAAudiofileProperties props;
	props.dSamplerate   = m_dSamplerate;
	props.eDomain       = ITA_TIME_DOMAIN;
	props.uiLength      = iLength;
	props.uiChannels    = 4;
	props.eQuantization = ITA_INT24;

	std::vector<const float*> v;
	v.push_back( pf1 );
	v.push_back( pf2 );
	v.push_back( pf3 );
	v.push_back( pf4 );
	writeAudiofile( sFilename, props, v );
}

void ITAQuadCTC::dumpSpectrum( const std::string& sFilename, ITAHDFTSpectrum& spSpectrum, bool bNorm )
{
	int n         = spSpectrum.getDFTSize( );
	float* pfTemp = fm_falloc( n );
	ITAFFT ifft( ITAFFT::IFFT_C2R, n, spSpectrum.data( ), pfTemp );
	ifft.execute( );
	// Normalisieren!
	float c = 1 / (float)n;

	if( bNorm )
	{
		float m = 0;
		for( int i = 0; i < n; i++ )
			m = ( std::max )( m, fabs( pfTemp[i] ) );
		c /= m;
	}

	fm_mul( pfTemp, c, n );
	writeAudiofile( sFilename, pfTemp, n, m_dSamplerate, ITA_INT24 );
	fm_free( pfTemp );
}

void ITAQuadCTC::dumpSpectrumText( const std::string& sFilename, ITAHDFTSpectrum& spSpectrum, bool bNorm )
{
	int n         = spSpectrum.getDFTSize( );
	float* pfTemp = fm_falloc( n );
	ITAFFT ifft( ITAFFT::IFFT_C2R, n, spSpectrum.data( ), pfTemp );
	ifft.execute( );
	// Normalisieren!
	float c = 1 / (float)n;

	if( bNorm )
	{
		float m = 0;
		for( int i = 0; i < n; i++ )
			m = ( std::max )( m, fabs( pfTemp[i] ) );
		c /= m;
	}

	fm_mul( pfTemp, c, n );

	/*
	std::string Data=spSpectrum.toString();
	std::ofstream SpectrumText;
	SpectrumText.open(sFilename.c_str());
	SpectrumText<<Data;
	SpectrumText.close();
	*/
	std::ofstream SpectrumText;
	SpectrumText.open( sFilename.c_str( ) );
	for( int i = 0; i < spSpectrum.getSize( ); i++ )
	{
		SpectrumText << spSpectrum.data( )[2 * i] << "\t\t" << spSpectrum.data( )[2 * i + 1] << std::endl;
	}
	SpectrumText.close( );


	// writeAudiofile(sFilename, pfTemp, n, m_dSamplerate, ITA_INT24);
	fm_free( pfTemp );
}


std::vector<std::pair<int, float> > ITAQuadCTC::determineConfig( )
{
	// Die drei Lautsprecher ermitteln, welche in geringster Azimuth-Entwerfung liegen

	m_csHead.init( m_headpos.vPos, m_headpos.vView, m_headpos.vUp );

	std::vector<std::pair<int, float> > vLSAzi;
	float fPhi, fDummy;

	for( int i = 0; i < 4; i++ )
	{
		m_csHead.getAnglesToTarget( m_csSpeaker[i].getPosition( ), fPhi, fDummy, fDummy );
		vLSAzi.push_back( std::pair<int, float>( i, correctAngle180( fPhi ) ) );

		// DEBUG:
#ifdef PRINT_INFOS
		DEBUG_PRINTF( "Kopf->LS%d: Azimuth %3.1f�\n", i + 1, correctAngle180( fPhi ) );
#endif
	}

	std::sort( vLSAzi.begin( ), vLSAzi.end( ), AngleSort );

	// DEBUG:
#ifdef PRINT_INFOS
	DEBUG_PRINTF( "Three azimuth-nearest speakers are: %d (%+0.1f deg), %d (%+0.1f deg), %d (%+0.1f deg)\n", vLSAzi[0].first + 1, vLSAzi[0].second, vLSAzi[1].first + 1,
	              vLSAzi[1].second, vLSAzi[2].first + 1, vLSAzi[2].second );
#endif

	/*
	 *  Die 90�-Anordnung ist nun gegeben durch die zwei Lautsprecher mit den
	 *  betragskleinsten Winkeln, also die Elemente vLSAzi[0] and vLSAzi[1].
	 *  Die 180�-Anordnung ist gegeben durch die Elemente vLSAzi[1] und vLSAzi[2].
	 */

	/*ACHTUNG:	Die Sortierung nach Winkel stimmt nur, wenn man sich im Mittelpunkt der
	 *			Lautsprecheranordnung befindet. Ansonsten muss nach Geometrieaspekten
	 *			sortiert werden. Der kleinste Winkel ist an erster Stelle,
	 *			der 2. kleinste an 2. Stelle, an 3. Stelle muss auf JEDEN Fall
	 *			der Lautsprecher gegen�ber dem 2. Lautsprecher!!!
	 */

	if( ( vLSAzi[1].first % 2 ) != ( vLSAzi[2].first % 2 ) )
		std::swap( vLSAzi[2], vLSAzi[3] );

	/*DEBUG_PRINTF("%d : %3.3f ; %d : %3.3f ; %d : %3.3f ; %d : %3.3f ;\n",
	             vLSAzi[0].first+5, vLSAzi[0].second,
	             vLSAzi[1].first+5, vLSAzi[1].second,
	             vLSAzi[2].first+5, vLSAzi[2].second,
	             vLSAzi[3].first+5, vLSAzi[3].second);
	*/


	/*
	 *  Nun die relativen Azimuth-Winkel f�r beide Konfigurationen bestimmen
	 *  (In der 90�-Anordnung entspricht 0� genau der Winkelhalbierenden zwischen den Lautsprechern)
	 */

	float fPhiBisec01 = vLSAzi[0].second + anglef_mindiff_0_360_DEG( vLSAzi[0].second, vLSAzi[1].second ) / 2;
	float fPhiBisec21 = vLSAzi[2].second + anglef_mindiff_0_360_DEG( vLSAzi[2].second, vLSAzi[1].second ) / 2;

	return vLSAzi;
}

void ITAQuadCTC::PreLoop( ) {}
void ITAQuadCTC::PostLoop( ) {}

bool ITAQuadCTC::LoopBody( )
{
	m_mxHeadData.Lock( );

	if( m_vHeadData.empty( ) )
	{
		m_mxHeadData.Unlock( );

		// Warten auf neue Kopfdaten
		m_evHeadDataAvail.WaitForEvent( true );

		m_mxHeadData.Lock( );
	}

	bool bCalculateFilter( false );

	if( !m_vHeadData.empty( ) )
	{
		// Neue Kopfdaten liegen vor
		// Logik: Wir nehmen nur den neusten Wert (sample latest)
		PositionViewUp& data = m_vHeadData.back( );

		// Neue Daten �berpr�fen
		if( m_bForceFilterUpdate || mustRecalculateFilters( data.m_vPos, data.m_vView, data.m_vUp ) )
		{
			// Neue Kopfposition setzen
			m_headpos.vPos  = data.m_vPos;
			m_headpos.vView = data.m_vView;
			m_headpos.vUp   = data.m_vUp;

			m_bForceFilterUpdate = false;

			// Filter neuberechnen
			bCalculateFilter = true;
		}
		// Queue leeren --> Outside???
		m_vHeadData.clear( );
	}

	m_evHeadDataAvail.ResetThisEvent( );
	m_mxHeadData.Unlock( );

	if( bCalculateFilter )
		calculateFilters( );

	// Stop-Flag �berpr�fen, ggf. selbst beenden
	if( m_bStopThread )
	{
		IndicateLoopEnd( );
		return false;
	}

	// Kein Wechsel zu einem Thread (no yield)
	return false;
}
