// $Id: Helpers.h,v 1.6 2008-12-02 22:31:36 fwefers Exp $

#ifndef __HELPERS_H__
#define __HELPERS_H__

#include <RG_Vector.h>
#include <map>

/*
//! Umrechnung eines Winkels im Bogenma� in Grad (float-Variante)
float rad2gradf(float phi);

//! Umrechnung eines Winkels im Grad in Bogenma� (float-Variante)
float grad2radf(float phi);
*/

//! Roll-Pitch-Yaw-Rotation eines Vektors durchf�hren
/**
 * F�hrt eine Rotation des gegebenen Vektors src im Roll-Pitch-Yaw-System durch.
 * Positive Winkel drehen um die entsprechende Achse gegen den Uhrzeigersinn (USZ)
 * (Blickrichtung Achsenpfeil nach Ursprung).
 *
 * Grundlage ist das OpenGL-Koordinatensystem (Rechth�ndig, Y-Achse nach oben).
 *
 * \note src und dest d�rfen gleich sein (in-place Berechnung)
 */
void RotateXYZ( const RG_Vector& src, RG_Vector& dest, float angleX, float angleY, float angleZ );

//! Cardanische Rotationen (Tait-Bryant Rotationen) eines Vektors durchf�hren
/**
 * F�hrt eine cardanische Rotation (auch Tait-Bryant Rotation) des gegebenen Vektors src durch.
 * Der Unterschied zur Euler-Rotation ist, das sich jede Achsen-Rotation auf die Objekt-Achsen
 * und nicht auf die Welt-Achsen bezieht.
 *
 * Grundlage ist das OpenGL-Koordinatensystem (Rechth�ndig, Y-Achse nach oben).
 *
 * heading: Rotation um die Z-Achse des Objektes (Drehung, yaw)
 * pitch:   Rotation um die X-Achse des Objektes (Neigung, pitch)
 * bank:    Rotation um die Y-Achse ...
 *
 * \note src und dest d�rfen gleich sein (in-place Berechnung)
 */
// void CardanTransform(const RG_Vector& src, RG_Vector& dest, float heading, float pitch, float bank);

void Angles_to_VUVec( float phi, float theta, float rho, RG_Vector& view, RG_Vector& up );

bool AngleSort( const std::pair<int, float>& a, const std::pair<int, float>& b );

#endif // __HELPERS_H__