// $Id: Helpers.cpp,v 1.6 2008-12-02 22:31:36 fwefers Exp $

#include "Helpers.h"

#include <ITANumericUtils.h>
#include <cmath>

static const float PI_F = std::acos( -1.0F );

/*
float rad2gradf(float phi) { return phi*180/PI_F; }
float grad2radf(float phi) { return phi*PI_F/180; }
*/

void EulerTransform( const RG_Vector& src, RG_Vector& dest, float phi, float theta, float rho )
{
	/*
	 *  Diese Formel f�r die Euler-Transformation wurde mit Maple hergeleitet anhand der
	 *  Definitionen im Buch "Real-time Rendering" von Tomas Akenine-M�ller und Eric Haines.
	 *
	 *  phi = Azimuth ("yaw"/"head")
	 *  theta = Elevation ("pitch")
	 *  rho = Rotation ("roll")
	 *
	 *  Die Euler-Transformationsmatrix E(phi, theta, rho) = R_z(rho)*R_x(theta)*R_y(phi) lautet in OpenGL Koordinaten dann
	 *
	 *      |  cos(rho)*cos(phi) + sin(rho)*sin(theta)*sin(phi), -sin(rho)*cos(theta), -cos(rho)*sin(phi) + sin(rho)*sin(theta)*cos(phi)  |
	 *      |                                                                                                                            |
	 *  E = |  sin(rho)*cos(phi) - cos(rho)*sin(theta)*sin(phi),  cos(rho)*cos(theta), -sin(rho)*sin(phi) - cos(rho)*sin(theta)*cos(phi)  |
	 *      |                                                                                                                            |
	 *      |  cos(theta)*sin(phi),                               sin(theta),          cos(theta)*cos(phi)                               |
	 *
	 *
	 *  (Das Bezugskoordinatensystem ist recht-h�ndig mit der Y-Achse nach oben (wie in OpenGL)
	 *  Positive Drehwinkel bewirken eine Drehung um die Achse entgegen dem Uhrzeigersinn bei
	 *  einer Blickrichtung Achsenpfeil nach Ursprung)
	 */

	// Eingabe-Vektor zwischenspeichern (f�r in-place Berechnung)
	RG_Vector v = src;

	// Trigenometrische Funktionen f�r die Winkel auswerten
	float sp = sin( phi ), cp = cos( phi );
	float st = sin( theta ), ct = cos( theta );
	float sr = sin( rho ), cr = cos( rho );

	// Matrix-Vektor-Produkt E(phi, theta, rho) * v berechnen
	dest[0] = ( cr * cp + sr * st * sp ) * v[0] - sr * ct * v[1] + ( -cr * sp + sr * st * cp ) * v[2];
	dest[1] = ( sr * cp - cr * st * sp ) * v[0] + cr * ct * v[1] + ( -sr * sp - cr * st * cp ) * v[2];
	dest[2] = ct * sp * v[0] + st * v[1] + ct * cp * v[2];
}

void Angles_to_VUVec( float phi, float theta, float rho, RG_Vector& view, RG_Vector& up )
{
	double vx, vy, vz, ux, uy, uz;
	convertYPR2VU( phi, theta, rho, vx, vy, vz, ux, uy, uz );
	view.init( (float)vx, (float)vy, (float)vz );
	up.init( (float)ux, (float)uy, (float)uz );
}

void RotateXYZ( const RG_Vector& src, RG_Vector& dest, float angleX, float angleY, float angleZ )
{
	/*
	 *  Diese Formel f�r eine Taite-Bryant-Rotation (auch bekannt als Roll-Pitch-Yaw) wurde
	 *  mit Maple hergeleitet, basierend auf den Rotationsmatrizes des R3. Unterschied zu den
	 *  Euler-Rotationen ist, das bei Taite-Bryant-Rotationen die Objektachsen als Bezugspunkt*
	 *  genommen werden und nicht die Koordinatenachsen.
	 *
	 *  Semantik (in Matritzen):  Ry(theta) * Rz(rho) * Rx(phi)
	 *
	 *  (Das Bezugskoordinatensystem ist recht-h�ndig mit der Y-Achse nach oben (wie in OpenGL)
	 *  Positive Drehwinkel bewirken eine Drehung um die Achse im Uhrzeigersinn bei
	 *  Blickrichtung Achsenpfeil)
	 */

	// Eingabe-Vektor zwischenspeichern (f�r in-place Berechnung)
	RG_Vector v = src;

	// Trigenometrische Funktionen f�r die Winkel auswerten
	// (alpha = Rotation um X-Achse = pitch)
	// (beta = Rotation um Y-Achse = roll)
	// (gamma = Rotation um Z-Achse = yaw)
	float sx = sin( angleX ), cx = cos( angleX );
	float sy = sin( angleY ), cy = cos( angleY );
	float sz = sin( angleZ ), cz = cos( angleZ );

	// Matrix-Vektor-Produkt E(phi, theta, rho) * v berechnen


	dest[0] = cy * cz * v[0] + ( -cx * cy * sz + sx * sy ) * v[1] + ( sx * cy * sz + cx * sy ) * v[2];
	dest[1] = sz * v[0] + cx * cz * v[1] - sx * cz * v[2];
	dest[2] = -sy * cz * v[0] + ( cx * sy * sz + sx * cy ) * v[1] + ( -sx * sy * sz + cx * cy ) * v[2];

	/*
	    dest[0] = (cy*cz + sx*sy*sz)*v[0] + (-cy*sz + sx*sy*cz)*v[1] + cx*sy*v[2];
	    dest[1] = cx*sz*v[0] + cx*cz*v[1] - sx*v[2];
	    dest[2] = (-sy*cz + sx*cy*sz)*v[0] + (sy*sz - sx*cy*cz)*v[1] + cx*cy*v[2];
	*/
}

bool AngleSort( const std::pair<int, float>& a, const std::pair<int, float>& b )
{
	return fabs( a.second ) < fabs( b.second );
}