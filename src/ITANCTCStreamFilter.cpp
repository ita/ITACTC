#include "ITANCTCStreamFilter.h"

#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITAHDFTSpectra.h>
#include <ITAHDFTSpectrum.h>
#include <ITASampleBuffer.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <ITAUPTrigger.h>
#include <assert.h>

ITANCTCStreamFilter::ITANCTCStreamFilter( const ITANCTCStreamFilter::Config& oC )
    : m_pdsInput( NULL )
    , m_pdsOutput( NULL )
    , m_pFilterPool( NULL )
    , m_pFilterExchangeTrigger( new ITAUPTrigger )
    , oConfig( oC )
{
	// Create filter pool; pre-fetch 2*N filters... arbitrary, done dynamically later
	m_pFilterPool = new ITAUPFilterPool( oConfig.iBlockLength, oConfig.iFilterLength, 2 * oConfig.N );

	// Create N two-channel convolvers
	for( int n = 0; n < oConfig.N; n++ )
	{
		ITAUPConvolution* pConvolverL = new ITAUPConvolution( oConfig.iBlockLength, oConfig.iFilterLength, m_pFilterPool );
		pConvolverL->SetFilterExchangeFadingFunction( oConfig.iFilterExchangeMode );
		pConvolverL->SetFilterCrossfadeLength( oConfig.iFilterCrossfadeLength );
		m_vpConvolvers.push_back( pConvolverL );

		ITAUPConvolution* pConvolverR = new ITAUPConvolution( oConfig.iBlockLength, oConfig.iFilterLength, m_pFilterPool );
		pConvolverR->SetFilterExchangeFadingFunction( oConfig.iFilterExchangeMode );
		pConvolverR->SetFilterCrossfadeLength( oConfig.iFilterCrossfadeLength );
		m_vpConvolvers.push_back( pConvolverR );
	}

	// Init output stream
	m_pdsOutput = new ITADatasourceRealization( oConfig.N, oConfig.dSampleRate, (unsigned int)oConfig.iBlockLength );
	m_pdsOutput->SetStreamEventHandler( this );
}

ITANCTCStreamFilter::~ITANCTCStreamFilter( )
{
	delete m_pdsOutput;
	for( int i = 0; i < 2 * oConfig.N; i++ )
		delete m_vpConvolvers[i];

	delete m_pFilterExchangeTrigger;
	delete m_pFilterPool;
}

void ITANCTCStreamFilter::SetInputDatasource( ITADatasource* pdsInput )
{
	if( pdsInput )
	{
		if( pdsInput->GetSampleRate( ) != oConfig.dSampleRate )
			ITA_EXCEPT1( INVALID_PARAMETER, "Datasource has different samplerate" );

		if( pdsInput->GetNumberOfChannels( ) != 2 )
			ITA_EXCEPT1( INVALID_PARAMETER, "Input datasource must be stereo" );

		if( int( pdsInput->GetBlocklength( ) ) != oConfig.iBlockLength )
			ITA_EXCEPT1( INVALID_PARAMETER, "Datasource has different streaming blocklength" );

		m_pdsInput = pdsInput;
	}

	return;
}

ITADatasource* ITANCTCStreamFilter::GetOutputDatasource( ) const
{
	return m_pdsOutput;
}

void ITANCTCStreamFilter::ExchangeFilters( const std::vector<ITABase::CHDFTSpectra*>& vpCTCFilter )
{
	// Helper (todo jst: use member variable for less alloc)
	ITASampleBuffer sbImpulseResponse( oConfig.iFilterLength, true );
	ITASampleBuffer sbSpectrum( oConfig.iFilterLength + 1, true );

	ITAFFT ifft( ITAFFT::IFFT_C2R, sbImpulseResponse.length( ), sbSpectrum.data( ), sbImpulseResponse.data( ) );
	float* fIn;
	float* fOut;

	for( int n = 0; n < oConfig.N; n++ )
	{
		const ITABase::CHDFTSpectra* oCTCFilter( vpCTCFilter[n] );

		const ITABase::CHDFTSpectrum& oCTCFilterL( *( *oCTCFilter )[0] );
		const ITABase::CHDFTSpectrum& oCTCFilterR( *( *oCTCFilter )[1] );

		int iIdxLeft  = 2 * n + 0;
		int iIdxRight = iIdxLeft + 1;

		// Left binaural signal
		ITAUPConvolution* pConvolverL( m_vpConvolvers[iIdxLeft] );
		ITAUPFilter* pNewFilterL = m_pFilterPool->RequestFilter( );
		sbSpectrum.write( oCTCFilterL.GetData( ), sbSpectrum.length( ) );
		fIn  = sbSpectrum.data( );
		fOut = sbImpulseResponse.data( );
		ifft.execute( fIn, fOut );
		sbImpulseResponse.div_scalar( float( sbImpulseResponse.length( ) ) );
		pNewFilterL->Load( sbImpulseResponse.data( ), sbImpulseResponse.length( ) );
		pConvolverL->ExchangeFilter( pNewFilterL );
		pNewFilterL->Release( );

		// Right binaural signal
		ITAUPConvolution* pConvolverR( m_vpConvolvers[iIdxRight] );
		ITAUPFilter* pNewFilterR = m_pFilterPool->RequestFilter( );
		sbSpectrum.write( oCTCFilterR.GetData( ), sbSpectrum.length( ) );
		fIn  = sbSpectrum.data( );
		fOut = sbImpulseResponse.data( );
		ifft.execute( fIn, fOut );
		sbImpulseResponse.div_scalar( float( sbImpulseResponse.length( ) ) );
		pNewFilterR->Load( sbImpulseResponse.data( ), sbImpulseResponse.length( ) );
		pConvolverR->ExchangeFilter( pNewFilterR );
		pNewFilterR->Release( );
	}

	return;
}

void ITANCTCStreamFilter::SetGains( const std::vector<float>& vfGains, bool bSetImmediately /*=false*/ )
{
	if( vfGains.size( ) != oConfig.N )
		ITA_EXCEPT1( INVALID_PARAMETER, "Gain vector has to be of size N" );

	for( int n = 0; n < oConfig.N; n++ )
	{
		int iIdxLeft  = 2 * n;
		int iIdxRight = iIdxLeft + 1;
		ITAUPConvolution* pConvolverL( m_vpConvolvers[iIdxLeft] );
		ITAUPConvolution* pConvolverR( m_vpConvolvers[iIdxRight] );

		const float& fGain( vfGains[n] );
		pConvolverL->SetGain( fGain, bSetImmediately );
		pConvolverR->SetGain( fGain, bSetImmediately );
	}
}

void ITANCTCStreamFilter::HandleProcessStream( ITADatasourceRealization*, const ITAStreamInfo* pStreamInfo )
{
	if( !m_pdsInput )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Input data source not defined yet" );

	const float* pfInputDataCh0 = m_pdsInput->GetBlockPointer( 0, pStreamInfo );
	const float* pfInputDataCh1 = m_pdsInput->GetBlockPointer( 1, pStreamInfo );

	// Iterate over loudspeakers and sum the corresponding 2 output channels from CTC
	std::vector<float*> vpOutputData;
	for( int n = 0; n < oConfig.N; n++ )
	{
		float* pfOutputData = m_pdsOutput->GetWritePointer( n );

		int iIdxLeft  = 2 * n;
		int iIdxRight = iIdxLeft + 1;
		ITAUPConvolution* pConvolverL( m_vpConvolvers[iIdxLeft] );
		ITAUPConvolution* pConvolverR( m_vpConvolvers[iIdxRight] );

		pConvolverL->Process( pfInputDataCh0, oConfig.iBlockLength, pfOutputData, oConfig.iBlockLength, ITABase::MixingMethod::OVERWRITE );
		pConvolverR->Process( pfInputDataCh1, oConfig.iBlockLength, pfOutputData, oConfig.iBlockLength, ITABase::MixingMethod::ADD );
	}

	// Increment write pointer on output stream
	m_pdsOutput->IncrementWritePointer( );
}

void ITANCTCStreamFilter::HandlePostIncrementBlockPointer( ITADatasourceRealization* )
{
	m_pdsInput->IncrementBlockPointer( );
}