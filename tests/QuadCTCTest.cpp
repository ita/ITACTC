
#include "Helpers.h"

#include <ITAException.h>
#include <ITAFileDatasource.h>
#include <ITAQuadCTC.h>
#include <RG_Vector.h>
#include <conio.h>
#include <iostream>
#include <stdio.h>

const static double dSampleRate = 44.1e3;
const static int iBlockSize     = 256;

/*
void testEulerTransform() {
    RG_Vector x, y;

    x.init(1, 0, 0);
    printf("\nx ");
    x.print();

    EulerTransform(x, y, grad2radf(45), 0, 0);
    printf("x rot phi=45");
    y.print();

    EulerTransform(x, y, 0, grad2radf(45), 0);
    printf("x rot theta=45");
    y.print();

    EulerTransform(x, y, 0, 0, grad2radf(45));
    printf("x rot rho=45");
    y.print();

    x.init(0, 1, 0);
    printf("\ny ");
    x.print();

    EulerTransform(x, y, grad2radf(45), 0, 0);
    printf("y rot phi=45");
    y.print();

    EulerTransform(x, y, 0, grad2radf(45), 0);
    printf("y rot theta=45");
    y.print();

    EulerTransform(x, y, 0, 0, grad2radf(45));
    printf("y rot rho=45");
    y.print();

    x.init(0, 0, 1);
    printf("\nz ");
    x.print();

    EulerTransform(x, y, grad2radf(45), 0, 0);
    printf("z rot phi=45");
    y.print();

    EulerTransform(x, y, 0, grad2radf(45), 0);
    printf("z rot theta=45");
    y.print();

    EulerTransform(x, y, 0, 0, grad2radf(45));
    printf("z rot rho=45");
    y.print();

}
*/

int main( int argc, char* argv[] )
{
	float azimuth;
	float headpos;
	// testEulerTransform();
	if( argc == 2 )
	{
		azimuth = (float)atoi( argv[1] );
		std::cout << "Azimuth-angle: " << azimuth << std::endl;
	}
	else if( argc == 3 )
	{
		azimuth = (float)atoi( argv[1] );
		std::cout << "Azimuth-angle: " << azimuth << std::endl;
		headpos = (float)atoi( argv[2] );
		std::cout << "Headposition relative to LS: " << headpos << std::endl;
	}
	else
		azimuth = 0;

	printf( "Calculating CTC-Filters for azimuth %f\n", azimuth );

	ITAQuadCTC* ctc = NULL;
	try
	{
		ITAFileDatasource ds( "ACDC.wav", 128 );

		// old version
		// ctc = new ITAQuadCTC(&ds, "square.ini", NULL, NULL);

		// New version
		ctc = new ITAQuadCTC( dSampleRate, iBlockSize, "square.ini" );

		// ctc.testLS2H(RG_Vector(0, 0, 0));


		// ctc.updateHeadPosition(RG_Vector(0,0,0),
		//	                   RG_Vector(0,0,1),
		//					   RG_Vector(0,1,0));
		if( argc == 2 )
		{
			ctc->updateHeadPosition( 0, 0, 0, azimuth, 0, 0 );
		}
		else if( argc == 3 )
		{
			ctc->updateHeadPosition( 0, headpos, 0, azimuth, 0, 0 );
		}
		else
			ctc->updateHeadPosition( 0, 0, 0, 180, 0, 0 );

		/*for (int i=0; i<100; i++) {
		ctc->updateHeadPosition(0, 0, 0, -40, 0, 0);
		ctc->updateHeadPosition(0, 2, 0, -40, 0, 0);
		}
		*/
		// ctc.visualize();
		// ctc.testHRIR();

		delete ctc;
	}
	catch( ITAException& e )
	{
		delete ctc;
		fprintf( stderr, "Error: %s\n", e.toString( ).c_str( ) );
		_getch( );
		return 255;
	}


	_getch( );

	return 0;
}


// int main(int argc, char* argv[]) { return testDSMBC(argc, argv); }