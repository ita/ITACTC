// $Id: ^NCTC_CTCFilterTest.cpp,v 1.1 2008-11-19 00:14:45 stienen Exp $

#include <DAFF.h>
#include <DSMBCConvolver.h>
#include <ITAAsioInterface.h>
#include <ITAConstants.h>
#include <ITAFileDatasource.h>
#include <ITAFilesystemUtils.h>
#include <ITANCTC.h>
#include <ITANCTCStreamFilter.h>
#include <ITAStringUtils.h>
#include <assert.h>
#include <conio.h>
#include <iostream>

// const static std::string sASIODriver = "ASIO Hammerfall DSP";
const static std::string sASIODriver = "ASIO4ALL v2";
// const static std::string sASIODriver = "M-Audio USB 2.0 ASIO";
const static double dSampleRate = 44.1e3;
const static int iBlockLength   = 128;

int main( int argc, char* argv[] )
{
	// HRIR
	std::string sHRIRFileName = correctPath( "../../../../VAData/HRIR/ITA-Kunstkopf_HRIR_AP11_Pressure_Equalized_3x3_256.daff" );

	// Configuration

	ITANCTC::Config oNCTCConfig;
	oNCTCConfig.N                = 4;
	oNCTCConfig.iCTCFilterLength = 2048;
	oNCTCConfig.dSampleRate      = 44.1e3;
	oNCTCConfig.fSpeedOfSound    = 344.0f;

	// 4 loudspeaker configuration (square with LS in corners facing center point)
	float fHeight   = 1.8f; // meter
	float fDistance = 2.0f; // meter
	ITANCTC::Pose oLSPose;

	// LS 0:
	oLSPose.vPos.SetValues( std::sqrt( fDistance ), fHeight, -std::sqrt( fDistance ) );
	oLSPose.vView.SetValues( 0, 0, -1.0f );
	oLSPose.vUp.SetValues( 0, 1.0f, 0 );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	// LS 1:
	oLSPose.vPos.SetValues( -std::sqrt( fDistance ), fHeight, -std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	// LS 2:
	oLSPose.vPos.SetValues( -std::sqrt( fDistance ), fHeight, std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	// LS 3:
	oLSPose.vPos.SetValues( std::sqrt( fDistance ), fHeight, std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );


	// DAFF HRIR
	DAFFReader* r = DAFFReader::create( );
	int ec        = r->openFile( sHRIRFileName );
	if( ec != 0 )
	{
		std::cerr << "DAFF Error: " << DAFFUtils::StrError( ec ) << std::endl;
		return 255;
	}
	DAFFContentIR* pHRIR = dynamic_cast<DAFFContentIR*>( r->getContent( ) );


	// Head pose
	ITANCTC::Pose oHead;
	oHead.vPos.SetToZeroVector( );
	// oHead.qOrient.SetToNeutralQuaternion();
	oHead.vView = Vista::ViewVector;
	oHead.vUp   = Vista::UpVector;


	// --- CircularNCTC ---

	// Instanciate
	ITANCTC ctc( oNCTCConfig );

	// Settings
	ctc.SetBeta( 0.001f );

	// Update and calculate filter
	ctc.SetHRIR( pHRIR );
	ctc.UpdateHeadPose( oHead );

	std::vector<ITAHDFTSpectra*> vpCTCFilter;
	for( int n = 0; n < ctc.GetN( ); n++ )
		vpCTCFilter.push_back( new ITAHDFTSpectra( oNCTCConfig.dSampleRate, 2, oNCTCConfig.iCTCFilterLength + 1, true ) );
	ctc.CalculateFilter( vpCTCFilter );

	std::string sFilePath = correctPath( "../../../tests/binaural_pferd.wav" );
	ITAFileDatasource dsBinauralSignal( sFilePath, iBlockLength, true );

	ITANCTCStreamFilter::Config oCTCStreamConfig;
	oCTCStreamConfig.N                      = ctc.GetN( );
	oCTCStreamConfig.dSampleRate            = dSampleRate;
	oCTCStreamConfig.iBlockLength           = iBlockLength;
	oCTCStreamConfig.iFilterCrossfadeLength = iBlockLength;
	oCTCStreamConfig.iFilterExchangeMode    = DSMBCConvolver::CROSSFADE_COSINE_SQUARE;
	oCTCStreamConfig.iFilterLength          = oNCTCConfig.iCTCFilterLength;
	ITANCTCStreamFilter ctcfiltering( oCTCStreamConfig );
	ctcfiltering.SetInputDatasource( &dsBinauralSignal );

	try
	{
		ASIOError ae;
		ITAsioInitializeLibrary( );
		ae = ITAsioInitializeDriver( sASIODriver.c_str( ) );
		ae = ITAsioCreateBuffers( 0, ctc.GetN( ), iBlockLength );
		ae = ITAsioSetPlaybackDatasource( ctcfiltering.GetOutputDatasource( ) );
		ITAStopwatch sw;

		std::cout << "Press h for help" << std::endl;
		int c;
		bool bQuit    = false;
		bool bRunning = false;
		bool bStart   = false;
		bool bStop    = false;
		while( c = _getch( ) )
		{
			switch( c )
			{
				case 'q':
					bQuit = true;
					break;

				case 'p':
					bRunning ? bStop = true : bStart = true;
					break;

				case 'x':
					std::cout << "Exchanging filter" << std::endl;
					ctcfiltering.ExchangeFilters( vpCTCFilter );
					break;

				case 'c':
					std::cout << "Calculating filter ... " << std::endl;
					sw.start( );
					ctc.CalculateFilter( vpCTCFilter );
					std::cout << "done in " << sw.stop( ) * 1e3 << " ms" << std::endl;
					for( int n = 0; n < ctc.GetN( ); n++ )
						vpCTCFilter[n]->Export( "CTCFilter_Online_LS" + IntToString( int( n + 1 ) ) );
					break;

				case 'r':
					std::cout << "Rotating head, counter yaw" << std::endl;
					oHead.qOrient *= VistaQuaternion( VistaAxisAndAngle( Vista::UpVector, Vista::DegToRad( -15.0f ) ) );
					ctc.UpdateHeadPose( oHead );
					break;

				case 'w':
					std::cout << "Rotating head, yaw" << std::endl;
					oHead.qOrient *= VistaQuaternion( VistaAxisAndAngle( Vista::UpVector, Vista::DegToRad( 15.0f ) ) );
					ctc.UpdateHeadPose( oHead );
					break;

				default:
					std::cout << "If you don't hear anything, calculate (c) and exchange filters (x) first, then start playback (p)." << std::endl;
					std::cout << "usage:" << std::endl;
					std::cout << "\tq: quit" << std::endl;
					std::cout << "\tc: force CTC filter recalculation (without exchange)" << std::endl;
					std::cout << "\tx: force CTC filter exchange (without prior recalc of CTC filter)" << std::endl;
					std::cout << "\tr: rotate head (counter yaw, no recalc, use 'c' then 'x')" << std::endl;
					std::cout << "\tw: rotate head (yaw, no recalc, use 'c' then 'x')" << std::endl;
					std::cout << "\tp: toggle playback" << std::endl;
					continue;
			}

			if( bQuit )
				break;

			if( bStart )
			{
				std::cout << "Starting playback" << std::endl;
				ITAsioStart( );
				bRunning = true;
				bStart   = false;
			}

			if( bStop )
			{
				std::cout << "Stopping playback" << std::endl;
				ITAsioStop( );
				bRunning = false;
				bStop    = false;
			}
		}

		if( bRunning )
			ITAsioStop( );
		ITAsioFinalizeDriver( );
		ITAsioFinalizeLibrary( );
	}
	catch( const ITAException& e )
	{
		std::cout << e << std::endl;
		std::cout << "Exiting." << std::endl;

		return 255;
	}

	delete pHRIR;

	return 0;
}
