// $Id: QuadCTCStreamfilterFileTest.cpp,v 1.1 2008-11-19 00:14:45 fwefers Exp $

#include <DSMBCConvolver.h>
#include <DSMBCFilter.h>
#include <ITADatasourceUtils.h>
#include <ITAFileDatasink.h>
#include <ITAFileDatasource.h>
#include <ITAQuadCTCStreamfilter.h>
#include <iostream>

int main( int argc, char* argv[] )
{
	// const int N=1<<10;			// Filter length
	const int N          = 1024; // Filter length
	const unsigned int b = 128;  // Block length

	float delay0[N];
	float delay1[N];


	for( int i = 0; i < N; i++ )
		delay0[i] = 0;
	delay0[0] = 1;

	for( int i = 0; i < N; i++ )
		delay1[i] = 0;
	// delay1[N*3/4] = 0.5;
	delay1[N >> 1] = 1;

	ITAFileDatasource* fds          = new ITAFileDatasource( "ACDC.wav", b, true );
	ITAQuadCTCStreamfilter* xfilter = new ITAQuadCTCStreamfilter( fds, N, DSMBCConvolver::CROSSFADE_LINEAR, b );

	// WriteFromDatasourceToFile(xds, "out.wav", fds->GetCapacity(), 1, true, true);
	ITAFileDatasink* fsink = new ITAFileDatasink( "out.wav", xfilter );

	fsink->Transfer( 44100 * 1 );

	xfilter->setFilters( delay1, NULL, delay0, NULL, delay1, NULL, delay0, NULL );

	fsink->Transfer( 44100 * 1 );
	std::cout << fds->GetFileCapacity( ) << std::endl;
	for( int i = 0; i < ( fds->GetCapacity( ) / 1024 ); i++ )
	{
		xfilter->setFilters( delay1, delay0, delay1, delay0, delay1, delay0, delay1, delay0 );
		fsink->Transfer( N );
	}


	/*
	xfilter->loadFilter(0, 0, buf1);
	xfilter->loadFilter(1, 1, buf1);
	xfilter->exchangeFilters();
	fsink->Transfer(44100*2);

	xfilter->loadFilter(0, 2, buf1);
	xfilter->loadFilter(1, 2, buf1);
	xfilter->exchangeFilters();
	fsink->Transfer(44100*2);

/*
	xds->loadFilter(0, 0, buf2);
	xds->loadFilter(0, 2, buf1);
	xds->loadFilter(1, 1, buf1);
	xds->loadFilter(0, 3, buf1);
	xds->exchangeFilters();

	fsink->Transfer(44100);

	xds->loadFilter(0, 2, NULL);
	xds->loadFilter(0, 2, buf1);
	xds->exchangeFilters();

	fsink->Transfer(44100);

	xds->loadFilter(0, 0, NULL);
	xds->loadFilter(1, 2, NULL);
	xds->exchangeFilters();

	fsink->Transfer(44100);
*/

	delete fsink;

	delete xfilter;
	delete fds;

	return 0;
}
