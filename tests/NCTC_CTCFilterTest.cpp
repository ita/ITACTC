#include <DAFF.h>
#include <ITAFFTUtils.h>
#include <ITAFileSystemUtils.h>
#include <ITANCTC.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <iostream>

using namespace ITABase;

int main( int, char** )
{
	// HRIR
	// std::string sHRIRFileName = correctPath( "../../../../VAData/HRIR/ITA-Kunstkopf_HRIR_AP11_Pressure_Equalized_3x3_256.daff" );
	std::string sHRIRFileName = correctPath( "D:/Users/roecher/Arbeitsumgebung/VAData/HRIR/ITA-Kunstkopf_HRIR_Mess01_D180_1x1_256.daff" );
	// Configuration

	ITANCTC::Config oNCTCConfig;
	oNCTCConfig.iCTCFilterLength = 10000; // 2048;
	oNCTCConfig.fSampleRate      = 44.1e3;
	oNCTCConfig.fSpeedOfSound    = 344.0f;
	// oNCTCConfig.iOptimization = ITANCTC::Config::OPTIMIZATION_NONE;
	// oNCTCConfig.iOptimization = ITANCTC::Config::OPTIMIZATION_AIXCAVE_FULLY_CLOSED;

	// 4 loudspeaker configuration (square with LS in corners facing center point)
	/*float fHeight = 3.2f; // meter
	float fDistance = 8.0f; // meter
	ITANCTC::Pose oLSPose;
	oLSPose.vPos.SetValues( std::sqrt( fDistance ), fHeight, -std::sqrt( fDistance ) );
	//oLSPose.qOrient.SetToNeutralQuaternion();
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( -std::sqrt( fDistance ), fHeight, -std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( -std::sqrt( fDistance ), fHeight, std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( std::sqrt( fDistance ), fHeight, std::sqrt( fDistance ) );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( 0.0f, fHeight*2, 0.0f );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( 0.0f, -fHeight, 0.0f );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );*/


	// aixCAVE speaker position
	float fHeight = 1.65f; // meter
	ITANCTC::Pose oLSPose;
	oLSPose.vPos.SetValues( -2.555, 3.402, 0.0 );
	// oLSPose.qOrient.SetToNeutralQuaternion();
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( 0.0, 3.403, -2.55 );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( 2.560, 3.402, 0.0 );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );
	oLSPose.vPos.SetValues( 0, 3.401, 2.3 );
	oNCTCConfig.voLoudspeaker.push_back( ITANCTC::Config::Loudspeaker( oLSPose ) );


	// Use number of LS as N
	oNCTCConfig.N = int( oNCTCConfig.voLoudspeaker.size( ) );

	// DAFF HRIR
	DAFFReader* r = DAFFReader::create( );
	int ec        = r->openFile( sHRIRFileName );
	if( ec != 0 )
	{
		std::cerr << "DAFF Error: " << DAFFUtils::StrError( ec ) << std::endl;
		return 255;
	}
	DAFFContentIR* pHRIR = dynamic_cast<DAFFContentIR*>( r->getContent( ) );


	// Head pose
	ITANCTC::Pose oHead;
	oHead.vPos.SetValues( 0.0f, fHeight, 0.0f );
	oHead.vView.SetValues( 0.0f, 0.0f, -1.0f );
	oHead.vUp.SetValues( 0.0f, 1.0f, 0.0f );
	// oHead.qOrient = VistaQuaternion( VistaAxisAndAngle( VistaVector3D( 0, 1, 0 ), Vista::DegToRad( -45 ) ) );
	// oHead.qOrient.SetToNeutralQuaternion();

	// Validate with view/up
	//	VistaVector3D v3View = oHead.qOrient.GetViewDir();
	//	VistaVector3D v3Up   = oHead.qOrient.GetUpDir();

	// --- NCTC ---

	// Instanciate
	ITANCTC ctc( oNCTCConfig );

	// Settings
	ctc.SetRegularizationFactor( 0.001f );

	// Update and calculate filter
	ctc.SetHRIR( pHRIR );
	ctc.UpdateHeadPose( oHead );

	std::vector<CHDFTSpectra*> vpCTCFilter;
	for( int n = 0; n < ctc.GetNumChannels( ); n++ )
		vpCTCFilter.push_back( new CHDFTSpectra( oNCTCConfig.fSampleRate, 2, oNCTCConfig.iCTCFilterLength + 1, true ) );

	try
	{
		ctc.CalculateFilter( vpCTCFilter );
	}
	catch( ITAException& e )
	{
		std::cout << e << std::endl;
	}

	for( int i = 0; i < ctc.GetNumChannels( ); i++ )
		ITAFFTUtils::Export( vpCTCFilter[i], "CTCFilter_" + IntToString( i ) );

	ITAStopWatch sw;
	std::cout << "Starting loop ";
	for( int i = 0; i < 1000; i++ )
	{
		sw.start( );
		ctc.CalculateFilter( vpCTCFilter );
		sw.stop( );
		if( i % 100 == 0 )
			std::cout << ".";
	}
	std::cout << std::endl;

	std::cout << "Mean (deviation) processing time for CTC filter: " << sw.mean( ) * 1.0e3 << " ms";
	std::cout << " ( " << sw.std_deviation( ) * 1.0e6 << " us )" << std::endl;

	// Export HRTFs
	std::vector<CHDFTSpectra*> vpHRTF;

	ctc.GetHRTF( vpHRTF );


	// Export resulting filter set to WAV file
	for( int n = 0; n < ctc.GetNumChannels( ); n++ )
	{
		ITAFFTUtils::Export( vpCTCFilter[n], "CTCFilter_LS" + IntToString( int( n + 1 ) ) );
		ITAFFTUtils::Export( vpHRTF[n], "HRTFs_LS" + IntToString( int( n + 1 ) ) );
	}
	delete pHRIR;

	return 0;
}
