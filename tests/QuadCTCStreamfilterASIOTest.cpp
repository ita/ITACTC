// $Id: QuadCTCStreamfilterASIOTest.cpp,v 1.2 2008-12-01 13:31:22 fwefers Exp $

#include <DSMBCConvolver.h>
#include <DSMBCFilter.h>
#include <ITADatasourceUtils.h>
#include <ITAException.h>
#include <ITAFileDatasource.h>
#include <ITAQuadCTCStreamfilter.h>
#include <ITAsioInterface.h>
#include <conio.h>

int main( int argc, char* argv[] )
{
	if( argc != 3 )
	{
		fprintf( stderr, "Syntax: QuadCTCStreamfilterASIOTest ASIO-TREIBERNUMMER AUDIODATEI\n\n" );
		printf( "Verf�gbare ASIO-Treiber:\n\n" );
		for( long i = 0; i < ITAsioGetNumDrivers( ); i++ )
			printf( "\t[%d] %s\n", i + 1, ITAsioGetDriverName( i ) );
		return 255;
	}

	long lASIODriver = atoi( argv[1] ) - 1;
	if( ( lASIODriver < 0 ) || ( lASIODriver >= ITAsioGetNumDrivers( ) ) )
	{
		fprintf( stderr, "Fehler: Ung�ltige ASIO-Treibernummer\n" );
		return 255;
	}

	if( ITAsioInit( lASIODriver ) != ASE_OK )
	{
		fprintf( stderr, "Fehler: Konnte ASIO-Treiber nicht initialisieren\n" );
		return 255;
	}

	double dSamplerate;
	ITAsioGetSampleRate( &dSamplerate );

	long lDummy, lBuffersize;
	ITAsioGetBufferSize( &lDummy, &lDummy, &lDummy, &lBuffersize );
	if( lBuffersize < 128 )
		lBuffersize = 128;
	printf( "Samplerate = %0.3f kHz, buffersize = %d samples\n", dSamplerate / 1000.0, lBuffersize );

	if( ITAsioCreateBuffers( 0, 4, lBuffersize ) != ASE_OK )
	{
		fprintf( stderr, "Fehler: Konnte ASIO-Puffer nicht erzeugen\n" );
		return 255;
	}

	// ----------

	const int N = 4096; // Filter length
	float ir_dirac[N];
	float ir_zero[N];

	for( int i = 0; i < N; i++ )
		ir_dirac[i] = 0;
	ir_dirac[0] = 1.0;
	for( int i = 0; i < N; i++ )
		ir_zero[i] = 0;

	ITAFileDatasource* fds = NULL;
	try
	{
		fds = new ITAFileDatasource( argv[2], lBuffersize );
		fds->SetLoopMode( true );
	}
	catch( ITAException& e )
	{
		fprintf( stderr, "Fehler: %s\n", e.toString( ).c_str( ) );
		return 255;
	}

	ITAQuadCTCStreamfilter* xfilter = new ITAQuadCTCStreamfilter( fds, N, DSMBCConvolver::CROSSFADE_LINEAR, lBuffersize );

	ITAsioSetPlaybackDatasource( xfilter );

	if( ITAsioStart( ) != ASE_OK )
	{
		fprintf( stderr, "Fehler: Konnte Wiedergabe nicht starten\n" );
		return 255;
	}

	int c;
	std::vector<bool> b( 8 );
	bool set_filters;
	while( ( c = _getch( ) ) != 'q' )
	{
		// if (c == 'h') sampler->AddPlaybackByTimecode(SAMPLE_HIHAT, TRACK_HIHAT, 0);
		if( ( c >= '1' ) && ( c <= '8' ) )
		{
			int i = c - '1';
			b[i]  = !b[i];
			printf( "(%d) %s\n", i, b[i] ? "on" : "off" );
			set_filters = true;
		}

		if( c == 'r' )
		{
			for( int i = 0; i < 8; i++ )
				b[i] = false;
			printf( "Reset\n" );
			set_filters = true;
		}

		if( set_filters )
		{
			xfilter->setFilters( b[0] ? ir_dirac : NULL, b[1] ? ir_dirac : NULL, b[2] ? ir_dirac : NULL, b[3] ? ir_dirac : NULL, b[4] ? ir_dirac : NULL,
			                     b[5] ? ir_dirac : NULL, b[6] ? ir_dirac : NULL, b[7] ? ir_dirac : NULL );
			set_filters = false;
		}
	}

	if( ITAsioStop( ) != ASE_OK )
	{
		fprintf( stderr, "Fehler: Konnte ASIO-Treiber nicht initialisieren\n" );
		return 255;
	}

	ITAsioDisposeBuffers( );

	delete xfilter;
	delete fds;
}
