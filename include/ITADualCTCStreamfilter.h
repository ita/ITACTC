/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef __ITA_DUALCTC_STREAMFILTER_H__
#define __ITA_DUALCTC_STREAMFILTER_H__

#include <ITACriticalSection.h>
#include <ITADatasource.h>
#include <ITADatasourceRealization.h>
#include <ITAStopwatch.h>
#include <vector>
#include <windows.h>

class DSMBCConvolver;
class DSMBCFilter;
class DSMBCFilterPool;
class DSMBCTrigger;

/*
 *  Diese Klasse realisiert die Echtzeitfilterung f�r die Dual-CTC.
 *  Sie filtert das 2-kanalige Eingangssignal mit den CTC-Filtern
 *  aller 2 Lautsprecher (4 an der Zahl) und stellt Methoden f�r deren
 *  Filter-Aktualisierung bereit. Das Ausgangssignal eines jeden Lautsprechers
 *  besteht aus der Summe eines Filteranteils des linken Eingangssignals,
 *  sowie eines Filteranteils des rechten Eingangssignals. Daher gibt es
 *  4 individuelle Filter(impulsantworten).
 *
 *  Blackbox-Sicht:        CTC-Filter (4)
 *                            o o o o
 *                            | | | |
 *                            V V V V
 *                     +--------------------+
 *                     |                    |
 *  Eingang L o------->|        CTC         |-------->o Lautsprecher 1
 *  Eingang R o------->|    Streamfilter    |-------->o Lautsprecher 2
 *                     |                    |
 *                     +--------------------+
 *
 *
 *
 *  Die Filterstruktur ist wiefolgt: (Nomenklatur: CTC<X><Y>, X = Eingang, Y = Lautsprecher)
 *
 *                        +--------------+       +---+
 *                +------>| CTC-Filter00 |------>|   |
 *                |       +--------------+       |   |
 *                |                              | + |------>O Lautsprecher 1
 *  Eingang L O---+       +--------------+       |   |
 *                |   +-->| CTC-Filter10 |------>|   |
 *                |   |   +--------------+       +---+
 *  Eingang R O-------+
 *                |   |   +--------------+       +---+
 *                +------>| CTC-Filter01 |------>|   |
 *                    |   +--------------+       |   |
 *                    |                          | + |------>O Lautsprecher 2
 *                    |   +--------------+       |   |
 *                    +-->| CTC-Filter11 |------>|   |
 *                        +--------------+       +---+
 *
 *  Sie hat immer vier Ausgangskan�le und �bernimmt die Samplerate und Blockl�nge ihrer Eingangsquelle.
 *  Die Realisierung der Filterung erfolgt als gleichf�rmige Zerlegte partitionierte
 *  Overlap-save Blockfaltung im Frequenzbereich (uniform partitioned frequency-domain).
 *  Der Austausch aller CTC-Filter geschieht stets atomar (in einem Aufruf).
 */

class ITADualCTCStreamfilter : public ITADatasourceRealizationEventHandler
{
public:
	//! Konstruktor
	/**
	 * \param pdsInput 2-Kanalige Eingangsdatenquelle
	 * \param iFilterlength Anzahl Filterkoeffizienten aller CTC-Filter (muss/sollte 2er-Potenz sein)
	 * \param iFilterExchangeMode Austauschstrategie f�r alle Filter
	 * \param iFilterCrossfadeLength �berblendl�nge [Anzahl Samples] f�r alle Filter
	 */

	ITADualCTCStreamfilter( double dSamplerate, int iBlocklength, int iFilterlength, int iFilterExchangeMode, int iFilterCrossfadeLength );

	//! Destruktor
	~ITADualCTCStreamfilter( );

	// Eingangsdatenquelle setzen
	// (Hinweis: Muss genau zwei Kan�le haben)
	void setInputDatasource( ITADatasource* pdsInput );

	// Ausgangsdatenquelle zur�ckgeben
	ITADatasource* getOutputDatasource( ) const;

	//! Den kompletten Filtersatz austauschen
	/**
	 * Tauscht den kompletten Satz CTC-Filter aus.
	 * Damit der CTC-Filtergenerator direkt weiterarbeiten kann,
	 * kommt die Methode sofort zur�ck sobald der Austausch initiiert ist.
	 * Trotzdem ist die Funktion strikt nicht-reentrant und intern gegen
	 * reentrance abgesichert. Der erneute Eintritt in die Methode geschieht
	 * erst wenn der vorherige Filteraustausch abgeschlossen ist.
	 *
	 * \note F�r leere Filter d�rfen auch Nullzeiger �bergeben werden (Geschwindigkeitsvorteil)
	 */
	void setFilters( const float* pfCTCFilter00, const float* pfCTCFilter10, const float* pfCTCFilter01, const float* pfCTCFilter11
	                 //					const float* pfCTCFilter02, const float* pfCTCFilter12,
	                 //					const float* pfCTCFilter03, const float* pfCTCFilter13
	);

	// --= Implementierung der Hooks f�r "ITADatasourceRealization" =--

	void HandleProcessStream( ITADatasourceRealization* pSender, const ITAStreamInfo* pStreamInfo );
	void HandlePostIncrementBlockPointer( ITADatasourceRealization* pSender );

private:
	double m_dSamplerate; // Abtastrate [Hz]
	int m_iBlocklength;   // Streaming-Blockl�nge
	ITADatasource* m_pdsInput;
	ITADatasourceRealization* m_pdsOutput;
	int m_iFilterlength;
	DSMBCFilterPool* m_pFilterPool;
	DSMBCTrigger* m_pFilterExchangeTrigger;
	std::vector<DSMBCConvolver*> m_vpConvolvers; // 8 individuelle Blockfalter f�r jedes CTC-Filter

	typedef std::vector<DSMBCFilter*> FilterSet;
	std::vector<FilterSet> m_vNextFilters; // Queue: N�chste Filters�tze zum Austausch
	ITACriticalSection m_csNextFilters;    // Lock f�r Queue (siehe oben)

	// F�r interne Zeitnahme
	ITAStopwatch m_swLoadFilters;
	ITAStopwatch m_swConvolution;

	// Filter f�r einen Kanal setzen
	void setFilter( int iIndex, const float* pfFilter );
};

#endif // __ITA_DUALCTC_STREAMFILTER_H__
