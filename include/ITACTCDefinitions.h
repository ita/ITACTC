/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_CTC_DEFINITIONS
#define INCLUDE_WATCHER_ITA_CTC_DEFINITIONS

#if( defined WIN32 ) && !( defined ITA_CTC_STATIC )
#	ifdef ITA_CTC_EXPORT
#		define ITA_CTC_API __declspec( dllexport )
#	else
#		define ITA_CTC_API __declspec( dllimport )
#	endif
#else
#	define ITA_CTC_API
#endif

#endif // INCLUDE_WATCHER_ITA_CTC_DEFINITIONS
